package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.NumOfTrainMatch;
import hu.bme.mit.mdsd.railway.NumOfTrainMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate NumOfTrainMatcher in a type-safe way.
 * 
 * @see NumOfTrainMatcher
 * @see NumOfTrainMatch
 * 
 */
@SuppressWarnings("all")
public final class NumOfTrainQuerySpecification extends BaseGeneratedEMFQuerySpecification<NumOfTrainMatcher> {
  private NumOfTrainQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static NumOfTrainQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected NumOfTrainMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return NumOfTrainMatcher.on(engine);
  }
  
  @Override
  public NumOfTrainMatch newEmptyMatch() {
    return NumOfTrainMatch.newEmptyMatch();
  }
  
  @Override
  public NumOfTrainMatch newMatch(final Object... parameters) {
    return NumOfTrainMatch.newMatch();
  }
  
  private static class LazyHolder {
    private final static NumOfTrainQuerySpecification INSTANCE = make();
    
    public static NumOfTrainQuerySpecification make() {
      return new NumOfTrainQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static NumOfTrainQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.numOfTrain";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList();
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList();
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_e = body.getOrCreateVariableByName("e");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      	));
      	new TypeUnary(body, var_e, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Train"), "hu.bme.mit.mdsd.railway/Train");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
