package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.TrainIsNotOnNodeMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Node;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.trainIsNotOnNode pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class TrainIsNotOnNodeProcessor implements IMatchProcessor<TrainIsNotOnNodeMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pNode the value of pattern parameter node in the currently processed match
   * 
   */
  public abstract void process(final Node pNode);
  
  @Override
  public void process(final TrainIsNotOnNodeMatch match) {
    process(match.getNode());
  }
}
