package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.GetAllTrainPairsOnSempahorelessPathMatch;
import hu.bme.mit.mdsd.railway.GetAllTrainPairsOnSempahorelessPathMatcher;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedButNotSemaphoreQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.annotations.PAnnotation;
import org.eclipse.incquery.runtime.matchers.psystem.annotations.ParameterReference;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.BinaryTransitiveClosure;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate GetAllTrainPairsOnSempahorelessPathMatcher in a type-safe way.
 * 
 * @see GetAllTrainPairsOnSempahorelessPathMatcher
 * @see GetAllTrainPairsOnSempahorelessPathMatch
 * 
 */
@SuppressWarnings("all")
public final class GetAllTrainPairsOnSempahorelessPathQuerySpecification extends BaseGeneratedEMFQuerySpecification<GetAllTrainPairsOnSempahorelessPathMatcher> {
  private GetAllTrainPairsOnSempahorelessPathQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static GetAllTrainPairsOnSempahorelessPathQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected GetAllTrainPairsOnSempahorelessPathMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return GetAllTrainPairsOnSempahorelessPathMatcher.on(engine);
  }
  
  @Override
  public GetAllTrainPairsOnSempahorelessPathMatch newEmptyMatch() {
    return GetAllTrainPairsOnSempahorelessPathMatch.newEmptyMatch();
  }
  
  @Override
  public GetAllTrainPairsOnSempahorelessPathMatch newMatch(final Object... parameters) {
    return GetAllTrainPairsOnSempahorelessPathMatch.newMatch((railway.Train) parameters[0], (railway.Train) parameters[1], (railway.TrackElement) parameters[2], (railway.TrackElement) parameters[3]);
  }
  
  private static class LazyHolder {
    private final static GetAllTrainPairsOnSempahorelessPathQuerySpecification INSTANCE = make();
    
    public static GetAllTrainPairsOnSempahorelessPathQuerySpecification make() {
      return new GetAllTrainPairsOnSempahorelessPathQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static GetAllTrainPairsOnSempahorelessPathQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.getAllTrainPairsOnSempahorelessPath";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("t1","t2","e1","e2");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("t1", "railway.Train"),new PParameter("t2", "railway.Train"),new PParameter("e1", "railway.TrackElement"),new PParameter("e2", "railway.TrackElement"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_t1 = body.getOrCreateVariableByName("t1");
      	PVariable var_t2 = body.getOrCreateVariableByName("t2");
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_t1, "t1"),
      				
      		new ExportedParameter(body, var_t2, "t2"),
      				
      		new ExportedParameter(body, var_e1, "e1"),
      				
      		new ExportedParameter(body, var_e2, "e2")
      	));
      	new Inequality(body, var_t1, var_t2);
      	new Inequality(body, var_e1, var_e2);
      	new TypeBinary(body, CONTEXT, var_e1, var_t1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "train"), "hu.bme.mit.mdsd.railway/TrackElement.train");
      	new TypeBinary(body, CONTEXT, var_e2, var_t2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "train"), "hu.bme.mit.mdsd.railway/TrackElement.train");
      	new BinaryTransitiveClosure(body, new FlatTuple(var_e1, var_e2), DirectlyConnectedButNotSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	{
      	PAnnotation annotation = new PAnnotation("Constraint");
      	annotation.addAttribute("severity", "error");
      	annotation.addAttribute("location", new ParameterReference("e1"));
      	annotation.addAttribute("message", "Train cannot be on a node!");
      	addAnnotation(annotation);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
