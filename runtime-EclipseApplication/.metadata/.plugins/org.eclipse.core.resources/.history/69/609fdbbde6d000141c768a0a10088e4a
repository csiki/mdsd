package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.IsEveryTrainPathValidMatch;
import hu.bme.mit.mdsd.railway.util.IsEveryTrainPathValidQuerySpecification;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.incquery.runtime.api.IQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseMatcher;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.tuple.Tuple;
import org.eclipse.incquery.runtime.util.IncQueryLoggingUtil;

/**
 * Generated pattern matcher API of the hu.bme.mit.mdsd.railway.isEveryTrainPathValid pattern,
 * providing pattern-specific query methods.
 * 
 * <p>Use the pattern matcher on a given model via {@link #on(IncQueryEngine)},
 * e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}.
 * 
 * <p>Matches of the pattern will be represented as {@link IsEveryTrainPathValidMatch}.
 * 
 * <p>Original source:
 * <code><pre>
 * // a k�t dead-end k�z�tt legyen lehets�ges �sszek�ttet�s
 * 
 * pattern isEveryTrainPathValid() {	 		// futtatni
 * 	Train(t);
 * 	Deadend(e1);
 * 	Deadend(e2);
 * 	e1 != e2;
 * 	Train.route(t, e1); 
 * 	Train.route(t, e2); 
 * 	find directlyConnected+(e1, e2);
 * }
 * </pre></code>
 * 
 * @see IsEveryTrainPathValidMatch
 * @see IsEveryTrainPathValidProcessor
 * @see IsEveryTrainPathValidQuerySpecification
 * 
 */
@SuppressWarnings("all")
public class IsEveryTrainPathValidMatcher extends BaseMatcher<IsEveryTrainPathValidMatch> {
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * 
   */
  public static IsEveryTrainPathValidMatcher on(final IncQueryEngine engine) throws IncQueryException {
    // check if matcher already exists
    IsEveryTrainPathValidMatcher matcher = engine.getExistingMatcher(querySpecification());
    if (matcher == null) {
    	matcher = new IsEveryTrainPathValidMatcher(engine);
    	// do not have to "put" it into engine.matchers, reportMatcherInitialized() will take care of it
    }
    return matcher;
  }
  
  private final static Logger LOGGER = IncQueryLoggingUtil.getLogger(IsEveryTrainPathValidMatcher.class);
  
  /**
   * Initializes the pattern matcher over a given EMF model root (recommended: Resource or ResourceSet).
   * If a pattern matcher is already constructed with the same root, only a light-weight reference is returned.
   * The scope of pattern matching will be the given EMF model root and below (see FAQ for more precise definition).
   * The match set will be incrementally refreshed upon updates from this scope.
   * <p>The matcher will be created within the managed {@link IncQueryEngine} belonging to the EMF model root, so
   * multiple matchers will reuse the same engine and benefit from increased performance and reduced memory footprint.
   * @param emfRoot the root of the EMF containment hierarchy where the pattern matcher will operate. Recommended: Resource or ResourceSet.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead, e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}
   * 
   */
  @Deprecated
  public IsEveryTrainPathValidMatcher(final Notifier emfRoot) throws IncQueryException {
    this(IncQueryEngine.on(emfRoot));
  }
  
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead
   * 
   */
  @Deprecated
  public IsEveryTrainPathValidMatcher(final IncQueryEngine engine) throws IncQueryException {
    super(engine, querySpecification());
  }
  
  /**
   * Indicates whether the (parameterless) pattern matches or not.
   * @return true if the pattern has a valid match.
   * 
   */
  public boolean hasMatch() {
    return rawHasMatch(new Object[]{});
  }
  
  @Override
  protected IsEveryTrainPathValidMatch tupleToMatch(final Tuple t) {
    try {
    	return IsEveryTrainPathValidMatch.newMatch();
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in tuple not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected IsEveryTrainPathValidMatch arrayToMatch(final Object[] match) {
    try {
    	return IsEveryTrainPathValidMatch.newMatch();
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected IsEveryTrainPathValidMatch arrayToMatchMutable(final Object[] match) {
    try {
    	return IsEveryTrainPathValidMatch.newMutableMatch();
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  /**
   * @return the singleton instance of the query specification of this pattern
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IQuerySpecification<IsEveryTrainPathValidMatcher> querySpecification() throws IncQueryException {
    return IsEveryTrainPathValidQuerySpecification.instance();
  }
}
