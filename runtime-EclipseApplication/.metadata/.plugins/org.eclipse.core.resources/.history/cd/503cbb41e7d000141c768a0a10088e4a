package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.FindTrainsOnNodeMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Node;
import railway.Train;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.findTrainsOnNode pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class FindTrainsOnNodeProcessor implements IMatchProcessor<FindTrainsOnNodeMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pNode the value of pattern parameter node in the currently processed match
   * @param pTrain the value of pattern parameter train in the currently processed match
   * 
   */
  public abstract void process(final Node pNode, final Train pTrain);
  
  @Override
  public void process(final FindTrainsOnNodeMatch match) {
    process(match.getNode(), match.getTrain());
  }
}
