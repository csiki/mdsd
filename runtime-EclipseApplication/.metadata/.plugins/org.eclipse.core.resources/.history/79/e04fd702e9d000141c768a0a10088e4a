package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.IsMoreSemaphoreThanTrainMatch;
import hu.bme.mit.mdsd.railway.IsMoreSemaphoreThanTrainMatcher;
import hu.bme.mit.mdsd.railway.util.CountSemaphoreQuerySpecification;
import hu.bme.mit.mdsd.railway.util.CountTrainQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.IExpressionEvaluator;
import org.eclipse.incquery.runtime.matchers.psystem.IValueProvider;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.annotations.PAnnotation;
import org.eclipse.incquery.runtime.matchers.psystem.annotations.ParameterReference;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExpressionEvaluation;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.PatternMatchCounter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate IsMoreSemaphoreThanTrainMatcher in a type-safe way.
 * 
 * @see IsMoreSemaphoreThanTrainMatcher
 * @see IsMoreSemaphoreThanTrainMatch
 * 
 */
@SuppressWarnings("all")
public final class IsMoreSemaphoreThanTrainQuerySpecification extends BaseGeneratedEMFQuerySpecification<IsMoreSemaphoreThanTrainMatcher> {
  private IsMoreSemaphoreThanTrainQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IsMoreSemaphoreThanTrainQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected IsMoreSemaphoreThanTrainMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return IsMoreSemaphoreThanTrainMatcher.on(engine);
  }
  
  @Override
  public IsMoreSemaphoreThanTrainMatch newEmptyMatch() {
    return IsMoreSemaphoreThanTrainMatch.newEmptyMatch();
  }
  
  @Override
  public IsMoreSemaphoreThanTrainMatch newMatch(final Object... parameters) {
    return IsMoreSemaphoreThanTrainMatch.newMatch((railway.MAV) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static IsMoreSemaphoreThanTrainQuerySpecification INSTANCE = make();
    
    public static IsMoreSemaphoreThanTrainQuerySpecification make() {
      return new IsMoreSemaphoreThanTrainQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static IsMoreSemaphoreThanTrainQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.isMoreSemaphoreThanTrain";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("mav");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("mav", "railway.MAV"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_mav = body.getOrCreateVariableByName("mav");
      	PVariable var_n = body.getOrCreateVariableByName("n");
      	PVariable var_m = body.getOrCreateVariableByName("m");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_mav, "mav")
      	));
      	new TypeUnary(body, var_mav, getClassifierLiteral("hu.bme.mit.mdsd.railway", "MAV"), "hu.bme.mit.mdsd.railway/MAV");
      	new PatternMatchCounter(body, new FlatTuple(), CountSemaphoreQuerySpecification.instance().getInternalQueryRepresentation(), var_n);
      	new PatternMatchCounter(body, new FlatTuple(), CountTrainQuerySpecification.instance().getInternalQueryRepresentation(), var_m);
      new ExpressionEvaluation(body, new IExpressionEvaluator() {
      	
      	@Override
      	public String getShortDescription() {
      		return "Expression evaluation from pattern isMoreSemaphoreThanTrain";
      	}
      
      	@Override
      	public Iterable<String> getInputParameterNames() {
      		return Arrays.asList("m", "n");
      	}
      
      	@Override
      	public Object evaluateExpression(IValueProvider provider) throws Exception {
      			java.lang.Integer m = (java.lang.Integer) provider.getValue("m");
      			java.lang.Integer n = (java.lang.Integer) provider.getValue("n");
      			return evaluateExpression_1_1(m, n);
      		}
      
      },  null); 
      	bodies.add(body);
      }
      {
      	PBody body = new PBody(this);
      	PVariable var_mav = body.getOrCreateVariableByName("mav");
      	PVariable var_m = body.getOrCreateVariableByName("m");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_mav, "mav")
      	));
      	new TypeUnary(body, var_mav, getClassifierLiteral("hu.bme.mit.mdsd.railway", "MAV"), "hu.bme.mit.mdsd.railway/MAV");
      	new PatternMatchCounter(body, new FlatTuple(), CountTrainQuerySpecification.instance().getInternalQueryRepresentation(), var_m);
      new ExpressionEvaluation(body, new IExpressionEvaluator() {
      	
      	@Override
      	public String getShortDescription() {
      		return "Expression evaluation from pattern isMoreSemaphoreThanTrain";
      	}
      
      	@Override
      	public Iterable<String> getInputParameterNames() {
      		return Arrays.asList("m");
      	}
      
      	@Override
      	public Object evaluateExpression(IValueProvider provider) throws Exception {
      			java.lang.Integer m = (java.lang.Integer) provider.getValue("m");
      			return evaluateExpression_2_1(m);
      		}
      
      },  null); 
      	bodies.add(body);
      }
      	{
      	PAnnotation annotation = new PAnnotation("Constraint");
      	annotation.addAttribute("severity", "error");
      	annotation.addAttribute("location", new ParameterReference("mav"));
      	annotation.addAttribute("message", "Semaphores either facing each other, or facing away from each other!");
      	addAnnotation(annotation);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
  
  private static boolean evaluateExpression_1_1(final Integer m, final Integer n) {
    boolean _greaterThan = (n.compareTo(m) > 0);
    return _greaterThan;
  }
  
  private static boolean evaluateExpression_2_1(final Integer m) {
    return ((m).intValue() == 1);
  }
}
