package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.IsThreeDeadEndInTrainPathMatch;
import hu.bme.mit.mdsd.railway.util.IsThreeDeadEndInTrainPathQuerySpecification;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import org.eclipse.incquery.runtime.api.IQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseMatcher;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.tuple.Tuple;
import org.eclipse.incquery.runtime.util.IncQueryLoggingUtil;
import railway.Train;

/**
 * Generated pattern matcher API of the hu.bme.mit.mdsd.railway.isThreeDeadEndInTrainPath pattern,
 * providing pattern-specific query methods.
 * 
 * <p>Use the pattern matcher on a given model via {@link #on(IncQueryEngine)},
 * e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}.
 * 
 * <p>Matches of the pattern will be represented as {@link IsThreeDeadEndInTrainPathMatch}.
 * 
 * <p>Original source:
 * <code><pre>
 * pattern isThreeDeadEndInTrainPath(t : Train) {
 * 	Deadend(e1);
 * 	Deadend(e2);
 * 	Deadend(e3);
 * 	e1 != e2;
 * 	e1 != e3;
 * 	e2 != e3;
 * 	Train.route(t, e1); 
 * 	Train.route(t, e2); 
 * 	Train.route(t, e3); 
 * }
 * </pre></code>
 * 
 * @see IsThreeDeadEndInTrainPathMatch
 * @see IsThreeDeadEndInTrainPathProcessor
 * @see IsThreeDeadEndInTrainPathQuerySpecification
 * 
 */
@SuppressWarnings("all")
public class IsThreeDeadEndInTrainPathMatcher extends BaseMatcher<IsThreeDeadEndInTrainPathMatch> {
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * 
   */
  public static IsThreeDeadEndInTrainPathMatcher on(final IncQueryEngine engine) throws IncQueryException {
    // check if matcher already exists
    IsThreeDeadEndInTrainPathMatcher matcher = engine.getExistingMatcher(querySpecification());
    if (matcher == null) {
    	matcher = new IsThreeDeadEndInTrainPathMatcher(engine);
    	// do not have to "put" it into engine.matchers, reportMatcherInitialized() will take care of it
    }
    return matcher;
  }
  
  private final static int POSITION_T = 0;
  
  private final static Logger LOGGER = IncQueryLoggingUtil.getLogger(IsThreeDeadEndInTrainPathMatcher.class);
  
  /**
   * Initializes the pattern matcher over a given EMF model root (recommended: Resource or ResourceSet).
   * If a pattern matcher is already constructed with the same root, only a light-weight reference is returned.
   * The scope of pattern matching will be the given EMF model root and below (see FAQ for more precise definition).
   * The match set will be incrementally refreshed upon updates from this scope.
   * <p>The matcher will be created within the managed {@link IncQueryEngine} belonging to the EMF model root, so
   * multiple matchers will reuse the same engine and benefit from increased performance and reduced memory footprint.
   * @param emfRoot the root of the EMF containment hierarchy where the pattern matcher will operate. Recommended: Resource or ResourceSet.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead, e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}
   * 
   */
  @Deprecated
  public IsThreeDeadEndInTrainPathMatcher(final Notifier emfRoot) throws IncQueryException {
    this(IncQueryEngine.on(emfRoot));
  }
  
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead
   * 
   */
  @Deprecated
  public IsThreeDeadEndInTrainPathMatcher(final IncQueryEngine engine) throws IncQueryException {
    super(engine, querySpecification());
  }
  
  /**
   * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pT the fixed value of pattern parameter t, or null if not bound.
   * @return matches represented as a IsThreeDeadEndInTrainPathMatch object.
   * 
   */
  public Collection<IsThreeDeadEndInTrainPathMatch> getAllMatches(final Train pT) {
    return rawGetAllMatches(new Object[]{pT});
  }
  
  /**
   * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pT the fixed value of pattern parameter t, or null if not bound.
   * @return a match represented as a IsThreeDeadEndInTrainPathMatch object, or null if no match is found.
   * 
   */
  public IsThreeDeadEndInTrainPathMatch getOneArbitraryMatch(final Train pT) {
    return rawGetOneArbitraryMatch(new Object[]{pT});
  }
  
  /**
   * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
   * under any possible substitution of the unspecified parameters (if any).
   * @param pT the fixed value of pattern parameter t, or null if not bound.
   * @return true if the input is a valid (partial) match of the pattern.
   * 
   */
  public boolean hasMatch(final Train pT) {
    return rawHasMatch(new Object[]{pT});
  }
  
  /**
   * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pT the fixed value of pattern parameter t, or null if not bound.
   * @return the number of pattern matches found.
   * 
   */
  public int countMatches(final Train pT) {
    return rawCountMatches(new Object[]{pT});
  }
  
  /**
   * Executes the given processor on each match of the pattern that conforms to the given fixed values of some parameters.
   * @param pT the fixed value of pattern parameter t, or null if not bound.
   * @param processor the action that will process each pattern match.
   * 
   */
  public void forEachMatch(final Train pT, final IMatchProcessor<? super IsThreeDeadEndInTrainPathMatch> processor) {
    rawForEachMatch(new Object[]{pT}, processor);
  }
  
  /**
   * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pT the fixed value of pattern parameter t, or null if not bound.
   * @param processor the action that will process the selected match.
   * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
   * 
   */
  public boolean forOneArbitraryMatch(final Train pT, final IMatchProcessor<? super IsThreeDeadEndInTrainPathMatch> processor) {
    return rawForOneArbitraryMatch(new Object[]{pT}, processor);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pT the fixed value of pattern parameter t, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public IsThreeDeadEndInTrainPathMatch newMatch(final Train pT) {
    return IsThreeDeadEndInTrainPathMatch.newMatch(pT);
  }
  
  /**
   * Retrieve the set of values that occur in matches for t.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  protected Set<Train> rawAccumulateAllValuesOft(final Object[] parameters) {
    Set<Train> results = new HashSet<Train>();
    rawAccumulateAllValues(POSITION_T, parameters, results);
    return results;
  }
  
  /**
   * Retrieve the set of values that occur in matches for t.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Train> getAllValuesOft() {
    return rawAccumulateAllValuesOft(emptyArray());
  }
  
  @Override
  protected IsThreeDeadEndInTrainPathMatch tupleToMatch(final Tuple t) {
    try {
    	return IsThreeDeadEndInTrainPathMatch.newMatch((railway.Train) t.get(POSITION_T));
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in tuple not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected IsThreeDeadEndInTrainPathMatch arrayToMatch(final Object[] match) {
    try {
    	return IsThreeDeadEndInTrainPathMatch.newMatch((railway.Train) match[POSITION_T]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected IsThreeDeadEndInTrainPathMatch arrayToMatchMutable(final Object[] match) {
    try {
    	return IsThreeDeadEndInTrainPathMatch.newMutableMatch((railway.Train) match[POSITION_T]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  /**
   * @return the singleton instance of the query specification of this pattern
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IQuerySpecification<IsThreeDeadEndInTrainPathMatcher> querySpecification() throws IncQueryException {
    return IsThreeDeadEndInTrainPathQuerySpecification.instance();
  }
}
