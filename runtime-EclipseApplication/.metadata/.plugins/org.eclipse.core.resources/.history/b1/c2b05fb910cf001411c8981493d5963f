package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.NoDangerousSempahorePairsMatch;
import hu.bme.mit.mdsd.railway.NoDangerousSempahorePairsMatcher;
import hu.bme.mit.mdsd.railway.util.IsDangerouslyConnectedQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate NoDangerousSempahorePairsMatcher in a type-safe way.
 * 
 * @see NoDangerousSempahorePairsMatcher
 * @see NoDangerousSempahorePairsMatch
 * 
 */
@SuppressWarnings("all")
public final class NoDangerousSempahorePairsQuerySpecification extends BaseGeneratedEMFQuerySpecification<NoDangerousSempahorePairsMatcher> {
  private NoDangerousSempahorePairsQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static NoDangerousSempahorePairsQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected NoDangerousSempahorePairsMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return NoDangerousSempahorePairsMatcher.on(engine);
  }
  
  @Override
  public NoDangerousSempahorePairsMatch newEmptyMatch() {
    return NoDangerousSempahorePairsMatch.newEmptyMatch();
  }
  
  @Override
  public NoDangerousSempahorePairsMatch newMatch(final Object... parameters) {
    return NoDangerousSempahorePairsMatch.newMatch();
  }
  
  private static class LazyHolder {
    private final static NoDangerousSempahorePairsQuerySpecification INSTANCE = make();
    
    public static NoDangerousSempahorePairsQuerySpecification make() {
      return new NoDangerousSempahorePairsQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static NoDangerousSempahorePairsQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.noDangerousSempahorePairs";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList();
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList();
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_s1 = body.getOrCreateVariableByName("s1");
      	PVariable var_s2 = body.getOrCreateVariableByName("s2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      	));
      	new TypeUnary(body, var_s1, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Semaphore"), "hu.bme.mit.mdsd.railway/Semaphore");
      	new TypeUnary(body, var_s2, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Semaphore"), "hu.bme.mit.mdsd.railway/Semaphore");
      	new NegativePatternCall(body, new FlatTuple(var_s1, var_s2), IsDangerouslyConnectedQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
