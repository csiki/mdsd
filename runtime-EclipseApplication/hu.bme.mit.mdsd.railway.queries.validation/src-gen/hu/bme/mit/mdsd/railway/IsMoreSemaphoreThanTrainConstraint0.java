package hu.bme.mit.mdsd.railway;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.incquery.validation.runtime.Constraint;
import org.eclipse.incquery.validation.runtime.ValidationUtil;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;

import hu.bme.mit.mdsd.railway.IsMoreSemaphoreThanTrainMatch;
import hu.bme.mit.mdsd.railway.util.IsMoreSemaphoreThanTrainQuerySpecification;
import hu.bme.mit.mdsd.railway.IsMoreSemaphoreThanTrainMatcher;

public class IsMoreSemaphoreThanTrainConstraint0 extends Constraint<IsMoreSemaphoreThanTrainMatch> {

	private IsMoreSemaphoreThanTrainQuerySpecification querySpecification;

	public IsMoreSemaphoreThanTrainConstraint0() throws IncQueryException {
		querySpecification = IsMoreSemaphoreThanTrainQuerySpecification.instance();
	}

	@Override
	public String getMessage() {
		return "Should be more semaphores on track than trains! Except when there\'s only one train.";
	}

	@Override
	public EObject getLocationObject(IsMoreSemaphoreThanTrainMatch signature) {
		Object location = signature.get("mav");
		if(location instanceof EObject){
			return (EObject) location;
		}
		return null;
	}

	@Override
	public int getSeverity() {
		return ValidationUtil.getSeverity("error");
	}

	@Override
	public BaseGeneratedEMFQuerySpecification<IsMoreSemaphoreThanTrainMatcher> getQuerySpecification() {
		return querySpecification;
	}
}
