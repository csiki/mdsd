/**
 */
package railway.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import railway.RailwayPackage;
import railway.Semaphore;
import railway.Signal;
import railway.TrackElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semaphore</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link railway.impl.SemaphoreImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link railway.impl.SemaphoreImpl#getState <em>State</em>}</li>
 *   <li>{@link railway.impl.SemaphoreImpl#getFacing <em>Facing</em>}</li>
 *   <li>{@link railway.impl.SemaphoreImpl#getNextState <em>Next State</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SemaphoreImpl extends NodeImpl implements Semaphore {
	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TrackElement> elements;

	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final Signal STATE_EDEFAULT = Signal.GO;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected Signal state = STATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFacing() <em>Facing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFacing()
	 * @generated
	 * @ordered
	 */
	protected TrackElement facing;

	/**
	 * The default value of the '{@link #getNextState() <em>Next State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextState()
	 * @generated
	 * @ordered
	 */
	protected static final Signal NEXT_STATE_EDEFAULT = Signal.GO;

	/**
	 * The cached value of the '{@link #getNextState() <em>Next State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextState()
	 * @generated
	 * @ordered
	 */
	protected Signal nextState = NEXT_STATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemaphoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RailwayPackage.Literals.SEMAPHORE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TrackElement> getElements() {
		if (elements == null) {
			elements = new EObjectResolvingEList<TrackElement>(TrackElement.class, this, RailwayPackage.SEMAPHORE__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Signal getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(Signal newState) {
		Signal oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RailwayPackage.SEMAPHORE__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrackElement getFacing() {
		if (facing != null && facing.eIsProxy()) {
			InternalEObject oldFacing = (InternalEObject)facing;
			facing = (TrackElement)eResolveProxy(oldFacing);
			if (facing != oldFacing) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RailwayPackage.SEMAPHORE__FACING, oldFacing, facing));
			}
		}
		return facing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrackElement basicGetFacing() {
		return facing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFacing(TrackElement newFacing) {
		TrackElement oldFacing = facing;
		facing = newFacing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RailwayPackage.SEMAPHORE__FACING, oldFacing, facing));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Signal getNextState() {
		return nextState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextState(Signal newNextState) {
		Signal oldNextState = nextState;
		nextState = newNextState == null ? NEXT_STATE_EDEFAULT : newNextState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RailwayPackage.SEMAPHORE__NEXT_STATE, oldNextState, nextState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RailwayPackage.SEMAPHORE__ELEMENTS:
				return getElements();
			case RailwayPackage.SEMAPHORE__STATE:
				return getState();
			case RailwayPackage.SEMAPHORE__FACING:
				if (resolve) return getFacing();
				return basicGetFacing();
			case RailwayPackage.SEMAPHORE__NEXT_STATE:
				return getNextState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RailwayPackage.SEMAPHORE__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends TrackElement>)newValue);
				return;
			case RailwayPackage.SEMAPHORE__STATE:
				setState((Signal)newValue);
				return;
			case RailwayPackage.SEMAPHORE__FACING:
				setFacing((TrackElement)newValue);
				return;
			case RailwayPackage.SEMAPHORE__NEXT_STATE:
				setNextState((Signal)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RailwayPackage.SEMAPHORE__ELEMENTS:
				getElements().clear();
				return;
			case RailwayPackage.SEMAPHORE__STATE:
				setState(STATE_EDEFAULT);
				return;
			case RailwayPackage.SEMAPHORE__FACING:
				setFacing((TrackElement)null);
				return;
			case RailwayPackage.SEMAPHORE__NEXT_STATE:
				setNextState(NEXT_STATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RailwayPackage.SEMAPHORE__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case RailwayPackage.SEMAPHORE__STATE:
				return state != STATE_EDEFAULT;
			case RailwayPackage.SEMAPHORE__FACING:
				return facing != null;
			case RailwayPackage.SEMAPHORE__NEXT_STATE:
				return nextState != NEXT_STATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (state: ");
		result.append(state);
		result.append(", nextState: ");
		result.append(nextState);
		result.append(')');
		return result.toString();
	}

} //SemaphoreImpl
