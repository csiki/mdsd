/**
 */
package railway.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import railway.MAV;
import railway.RailwayPackage;
import railway.TrackElement;
import railway.Train;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAV</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link railway.impl.MAVImpl#getTrackElements <em>Track Elements</em>}</li>
 *   <li>{@link railway.impl.MAVImpl#getTrains <em>Trains</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MAVImpl extends MinimalEObjectImpl.Container implements MAV {
	/**
	 * The cached value of the '{@link #getTrackElements() <em>Track Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TrackElement> trackElements;

	/**
	 * The cached value of the '{@link #getTrains() <em>Trains</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrains()
	 * @generated
	 * @ordered
	 */
	protected EList<Train> trains;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAVImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RailwayPackage.Literals.MAV;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TrackElement> getTrackElements() {
		if (trackElements == null) {
			trackElements = new EObjectContainmentEList<TrackElement>(TrackElement.class, this, RailwayPackage.MAV__TRACK_ELEMENTS);
		}
		return trackElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Train> getTrains() {
		if (trains == null) {
			trains = new EObjectContainmentEList<Train>(Train.class, this, RailwayPackage.MAV__TRAINS);
		}
		return trains;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RailwayPackage.MAV__TRACK_ELEMENTS:
				return ((InternalEList<?>)getTrackElements()).basicRemove(otherEnd, msgs);
			case RailwayPackage.MAV__TRAINS:
				return ((InternalEList<?>)getTrains()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RailwayPackage.MAV__TRACK_ELEMENTS:
				return getTrackElements();
			case RailwayPackage.MAV__TRAINS:
				return getTrains();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RailwayPackage.MAV__TRACK_ELEMENTS:
				getTrackElements().clear();
				getTrackElements().addAll((Collection<? extends TrackElement>)newValue);
				return;
			case RailwayPackage.MAV__TRAINS:
				getTrains().clear();
				getTrains().addAll((Collection<? extends Train>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RailwayPackage.MAV__TRACK_ELEMENTS:
				getTrackElements().clear();
				return;
			case RailwayPackage.MAV__TRAINS:
				getTrains().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RailwayPackage.MAV__TRACK_ELEMENTS:
				return trackElements != null && !trackElements.isEmpty();
			case RailwayPackage.MAV__TRAINS:
				return trains != null && !trains.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MAVImpl
