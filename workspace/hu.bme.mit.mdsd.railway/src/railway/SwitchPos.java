/**
 */
package railway;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Pos</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link railway.SwitchPos#getFrom <em>From</em>}</li>
 *   <li>{@link railway.SwitchPos#getTo <em>To</em>}</li>
 * </ul>
 * </p>
 *
 * @see railway.RailwayPackage#getSwitchPos()
 * @model
 * @generated
 */
public interface SwitchPos extends EObject {
	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(TrackElement)
	 * @see railway.RailwayPackage#getSwitchPos_From()
	 * @model required="true"
	 * @generated
	 */
	TrackElement getFrom();

	/**
	 * Sets the value of the '{@link railway.SwitchPos#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(TrackElement value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(TrackElement)
	 * @see railway.RailwayPackage#getSwitchPos_To()
	 * @model required="true"
	 * @generated
	 */
	TrackElement getTo();

	/**
	 * Sets the value of the '{@link railway.SwitchPos#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(TrackElement value);

} // SwitchPos
