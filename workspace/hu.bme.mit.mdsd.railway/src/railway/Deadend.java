/**
 */
package railway;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deadend</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link railway.Deadend#getSection <em>Section</em>}</li>
 * </ul>
 * </p>
 *
 * @see railway.RailwayPackage#getDeadend()
 * @model
 * @generated
 */
public interface Deadend extends Node {
	/**
	 * Returns the value of the '<em><b>Section</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Section</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Section</em>' reference.
	 * @see #setSection(TrackElement)
	 * @see railway.RailwayPackage#getDeadend_Section()
	 * @model required="true"
	 * @generated
	 */
	TrackElement getSection();

	/**
	 * Sets the value of the '{@link railway.Deadend#getSection <em>Section</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Section</em>' reference.
	 * @see #getSection()
	 * @generated
	 */
	void setSection(TrackElement value);

} // Deadend
