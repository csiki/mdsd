/**
 */
package railway;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link railway.Switch#getElements <em>Elements</em>}</li>
 *   <li>{@link railway.Switch#getConnections <em>Connections</em>}</li>
 * </ul>
 * </p>
 *
 * @see railway.RailwayPackage#getSwitch()
 * @model
 * @generated
 */
public interface Switch extends Node {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' reference list.
	 * The list contents are of type {@link railway.TrackElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' reference list.
	 * @see railway.RailwayPackage#getSwitch_Elements()
	 * @model lower="2"
	 * @generated
	 */
	EList<TrackElement> getElements();

	/**
	 * Returns the value of the '<em><b>Connections</b></em>' containment reference list.
	 * The list contents are of type {@link railway.SwitchPos}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connections</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connections</em>' containment reference list.
	 * @see railway.RailwayPackage#getSwitch_Connections()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<SwitchPos> getConnections();

} // Switch
