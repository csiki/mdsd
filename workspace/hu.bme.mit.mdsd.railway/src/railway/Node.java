/**
 */
package railway;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see railway.RailwayPackage#getNode()
 * @model abstract="true"
 * @generated
 */
public interface Node extends TrackElement {
} // Node
