package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.util.IsSemaphoreQuerySpecification;
import java.util.Arrays;
import java.util.List;
import org.eclipse.incquery.runtime.api.IPatternMatch;
import org.eclipse.incquery.runtime.api.impl.BasePatternMatch;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import railway.TrackElement;

/**
 * Pattern-specific match representation of the hu.bme.mit.mdsd.railway.isSemaphore pattern,
 * to be used in conjunction with {@link IsSemaphoreMatcher}.
 * 
 * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
 * Each instance is a (possibly partial) substitution of pattern parameters,
 * usable to represent a match of the pattern in the result of a query,
 * or to specify the bound (fixed) input parameters when issuing a query.
 * 
 * @see IsSemaphoreMatcher
 * @see IsSemaphoreProcessor
 * 
 */
@SuppressWarnings("all")
public abstract class IsSemaphoreMatch extends BasePatternMatch {
  private TrackElement fE;
  
  private static List<String> parameterNames = makeImmutableList("e");
  
  private IsSemaphoreMatch(final TrackElement pE) {
    this.fE = pE;
  }
  
  @Override
  public Object get(final String parameterName) {
    if ("e".equals(parameterName)) return this.fE;
    return null;
  }
  
  public TrackElement getE() {
    return this.fE;
  }
  
  @Override
  public boolean set(final String parameterName, final Object newValue) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    if ("e".equals(parameterName) ) {
    	this.fE = (railway.TrackElement) newValue;
    	return true;
    }
    return false;
  }
  
  public void setE(final TrackElement pE) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    this.fE = pE;
  }
  
  @Override
  public String patternName() {
    return "hu.bme.mit.mdsd.railway.isSemaphore";
  }
  
  @Override
  public List<String> parameterNames() {
    return IsSemaphoreMatch.parameterNames;
  }
  
  @Override
  public Object[] toArray() {
    return new Object[]{fE};
  }
  
  @Override
  public IsSemaphoreMatch toImmutable() {
    return isMutable() ? newMatch(fE) : this;
  }
  
  @Override
  public String prettyPrint() {
    StringBuilder result = new StringBuilder();
    result.append("\"e\"=" + prettyPrintValue(fE)
    );
    return result.toString();
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fE == null) ? 0 : fE.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
    	return true;
    if (!(obj instanceof IsSemaphoreMatch)) { // this should be infrequent
    	if (obj == null) {
    		return false;
    	}
    	if (!(obj instanceof IPatternMatch)) {
    		return false;
    	}
    	IPatternMatch otherSig  = (IPatternMatch) obj;
    	if (!specification().equals(otherSig.specification()))
    		return false;
    	return Arrays.deepEquals(toArray(), otherSig.toArray());
    }
    IsSemaphoreMatch other = (IsSemaphoreMatch) obj;
    if (fE == null) {if (other.fE != null) return false;}
    else if (!fE.equals(other.fE)) return false;
    return true;
  }
  
  @Override
  public IsSemaphoreQuerySpecification specification() {
    try {
    	return IsSemaphoreQuerySpecification.instance();
    } catch (IncQueryException ex) {
     	// This cannot happen, as the match object can only be instantiated if the query specification exists
     	throw new IllegalStateException (ex);
    }
  }
  
  /**
   * Returns an empty, mutable match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @return the empty match.
   * 
   */
  public static IsSemaphoreMatch newEmptyMatch() {
    return new Mutable(null);
  }
  
  /**
   * Returns a mutable (partial) match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @param pE the fixed value of pattern parameter e, or null if not bound.
   * @return the new, mutable (partial) match object.
   * 
   */
  public static IsSemaphoreMatch newMutableMatch(final TrackElement pE) {
    return new Mutable(pE);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pE the fixed value of pattern parameter e, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public static IsSemaphoreMatch newMatch(final TrackElement pE) {
    return new Immutable(pE);
  }
  
  private static final class Mutable extends IsSemaphoreMatch {
    Mutable(final TrackElement pE) {
      super(pE);
    }
    
    @Override
    public boolean isMutable() {
      return true;
    }
  }
  
  private static final class Immutable extends IsSemaphoreMatch {
    Immutable(final TrackElement pE) {
      super(pE);
    }
    
    @Override
    public boolean isMutable() {
      return false;
    }
  }
}
