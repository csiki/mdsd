package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.IsEveryTrainPathValidMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Train;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.isEveryTrainPathValid pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class IsEveryTrainPathValidProcessor implements IMatchProcessor<IsEveryTrainPathValidMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pT the value of pattern parameter t in the currently processed match
   * 
   */
  public abstract void process(final Train pT);
  
  @Override
  public void process(final IsEveryTrainPathValidMatch match) {
    process(match.getT());
  }
}
