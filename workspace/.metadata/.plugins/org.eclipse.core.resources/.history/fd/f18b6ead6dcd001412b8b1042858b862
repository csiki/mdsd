package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.TrainFromToDeadendMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Train;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.trainFromToDeadend pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class TrainFromToDeadendProcessor implements IMatchProcessor<TrainFromToDeadendMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pTrain the value of pattern parameter train in the currently processed match
   * 
   */
  public abstract void process(final Train pTrain);
  
  @Override
  public void process(final TrainFromToDeadendMatch match) {
    process(match.getTrain());
  }
}
