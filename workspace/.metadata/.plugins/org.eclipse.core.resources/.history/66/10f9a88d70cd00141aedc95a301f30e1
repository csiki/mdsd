package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.TrainIsNotOnThisNodeAllMatch;
import hu.bme.mit.mdsd.railway.util.TrainIsNotOnThisNodeAllQuerySpecification;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import org.eclipse.incquery.runtime.api.IQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseMatcher;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.tuple.Tuple;
import org.eclipse.incquery.runtime.util.IncQueryLoggingUtil;
import railway.Node;

/**
 * Generated pattern matcher API of the hu.bme.mit.mdsd.railway.trainIsNotOnThisNodeAll pattern,
 * providing pattern-specific query methods.
 * 
 * <p>Use the pattern matcher on a given model via {@link #on(IncQueryEngine)},
 * e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}.
 * 
 * <p>Matches of the pattern will be represented as {@link TrainIsNotOnThisNodeAllMatch}.
 * 
 * <p>Original source:
 * <code><pre>
 * pattern trainIsNotOnThisNodeAll(node : Node) {
 * 	neg find trainIsNotOnThisNode(node);
 * }
 * </pre></code>
 * 
 * @see TrainIsNotOnThisNodeAllMatch
 * @see TrainIsNotOnThisNodeAllProcessor
 * @see TrainIsNotOnThisNodeAllQuerySpecification
 * 
 */
@SuppressWarnings("all")
public class TrainIsNotOnThisNodeAllMatcher extends BaseMatcher<TrainIsNotOnThisNodeAllMatch> {
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * 
   */
  public static TrainIsNotOnThisNodeAllMatcher on(final IncQueryEngine engine) throws IncQueryException {
    // check if matcher already exists
    TrainIsNotOnThisNodeAllMatcher matcher = engine.getExistingMatcher(querySpecification());
    if (matcher == null) {
    	matcher = new TrainIsNotOnThisNodeAllMatcher(engine);
    	// do not have to "put" it into engine.matchers, reportMatcherInitialized() will take care of it
    }
    return matcher;
  }
  
  private final static int POSITION_NODE = 0;
  
  private final static Logger LOGGER = IncQueryLoggingUtil.getLogger(TrainIsNotOnThisNodeAllMatcher.class);
  
  /**
   * Initializes the pattern matcher over a given EMF model root (recommended: Resource or ResourceSet).
   * If a pattern matcher is already constructed with the same root, only a light-weight reference is returned.
   * The scope of pattern matching will be the given EMF model root and below (see FAQ for more precise definition).
   * The match set will be incrementally refreshed upon updates from this scope.
   * <p>The matcher will be created within the managed {@link IncQueryEngine} belonging to the EMF model root, so
   * multiple matchers will reuse the same engine and benefit from increased performance and reduced memory footprint.
   * @param emfRoot the root of the EMF containment hierarchy where the pattern matcher will operate. Recommended: Resource or ResourceSet.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead, e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}
   * 
   */
  @Deprecated
  public TrainIsNotOnThisNodeAllMatcher(final Notifier emfRoot) throws IncQueryException {
    this(IncQueryEngine.on(emfRoot));
  }
  
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead
   * 
   */
  @Deprecated
  public TrainIsNotOnThisNodeAllMatcher(final IncQueryEngine engine) throws IncQueryException {
    super(engine, querySpecification());
  }
  
  /**
   * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @return matches represented as a TrainIsNotOnThisNodeAllMatch object.
   * 
   */
  public Collection<TrainIsNotOnThisNodeAllMatch> getAllMatches(final Node pNode) {
    return rawGetAllMatches(new Object[]{pNode});
  }
  
  /**
   * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @return a match represented as a TrainIsNotOnThisNodeAllMatch object, or null if no match is found.
   * 
   */
  public TrainIsNotOnThisNodeAllMatch getOneArbitraryMatch(final Node pNode) {
    return rawGetOneArbitraryMatch(new Object[]{pNode});
  }
  
  /**
   * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
   * under any possible substitution of the unspecified parameters (if any).
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @return true if the input is a valid (partial) match of the pattern.
   * 
   */
  public boolean hasMatch(final Node pNode) {
    return rawHasMatch(new Object[]{pNode});
  }
  
  /**
   * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @return the number of pattern matches found.
   * 
   */
  public int countMatches(final Node pNode) {
    return rawCountMatches(new Object[]{pNode});
  }
  
  /**
   * Executes the given processor on each match of the pattern that conforms to the given fixed values of some parameters.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param processor the action that will process each pattern match.
   * 
   */
  public void forEachMatch(final Node pNode, final IMatchProcessor<? super TrainIsNotOnThisNodeAllMatch> processor) {
    rawForEachMatch(new Object[]{pNode}, processor);
  }
  
  /**
   * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param processor the action that will process the selected match.
   * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
   * 
   */
  public boolean forOneArbitraryMatch(final Node pNode, final IMatchProcessor<? super TrainIsNotOnThisNodeAllMatch> processor) {
    return rawForOneArbitraryMatch(new Object[]{pNode}, processor);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public TrainIsNotOnThisNodeAllMatch newMatch(final Node pNode) {
    return TrainIsNotOnThisNodeAllMatch.newMatch(pNode);
  }
  
  /**
   * Retrieve the set of values that occur in matches for node.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  protected Set<Node> rawAccumulateAllValuesOfnode(final Object[] parameters) {
    Set<Node> results = new HashSet<Node>();
    rawAccumulateAllValues(POSITION_NODE, parameters, results);
    return results;
  }
  
  /**
   * Retrieve the set of values that occur in matches for node.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Node> getAllValuesOfnode() {
    return rawAccumulateAllValuesOfnode(emptyArray());
  }
  
  @Override
  protected TrainIsNotOnThisNodeAllMatch tupleToMatch(final Tuple t) {
    try {
    	return TrainIsNotOnThisNodeAllMatch.newMatch((railway.Node) t.get(POSITION_NODE));
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in tuple not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected TrainIsNotOnThisNodeAllMatch arrayToMatch(final Object[] match) {
    try {
    	return TrainIsNotOnThisNodeAllMatch.newMatch((railway.Node) match[POSITION_NODE]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected TrainIsNotOnThisNodeAllMatch arrayToMatchMutable(final Object[] match) {
    try {
    	return TrainIsNotOnThisNodeAllMatch.newMutableMatch((railway.Node) match[POSITION_NODE]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  /**
   * @return the singleton instance of the query specification of this pattern
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IQuerySpecification<TrainIsNotOnThisNodeAllMatcher> querySpecification() throws IncQueryException {
    return TrainIsNotOnThisNodeAllQuerySpecification.instance();
  }
}
