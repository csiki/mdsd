package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.IsConnectedMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Semaphore;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.isConnected pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class IsConnectedProcessor implements IMatchProcessor<IsConnectedMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pS1 the value of pattern parameter s1 in the currently processed match
   * @param pS2 the value of pattern parameter s2 in the currently processed match
   * 
   */
  public abstract void process(final Semaphore pS1, final Semaphore pS2);
  
  @Override
  public void process(final IsConnectedMatch match) {
    process(match.getS1(), match.getS2());
  }
}
