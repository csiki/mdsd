package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.TrainIsNotOnThisNodeMatch;
import hu.bme.mit.mdsd.railway.TrainIsNotOnThisNodeMatcher;
import hu.bme.mit.mdsd.railway.util.FindTrainsOnNodeQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate TrainIsNotOnThisNodeMatcher in a type-safe way.
 * 
 * @see TrainIsNotOnThisNodeMatcher
 * @see TrainIsNotOnThisNodeMatch
 * 
 */
@SuppressWarnings("all")
public final class TrainIsNotOnThisNodeQuerySpecification extends BaseGeneratedEMFQuerySpecification<TrainIsNotOnThisNodeMatcher> {
  private TrainIsNotOnThisNodeQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static TrainIsNotOnThisNodeQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected TrainIsNotOnThisNodeMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return TrainIsNotOnThisNodeMatcher.on(engine);
  }
  
  @Override
  public TrainIsNotOnThisNodeMatch newEmptyMatch() {
    return TrainIsNotOnThisNodeMatch.newEmptyMatch();
  }
  
  @Override
  public TrainIsNotOnThisNodeMatch newMatch(final Object... parameters) {
    return TrainIsNotOnThisNodeMatch.newMatch((railway.Node) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static TrainIsNotOnThisNodeQuerySpecification INSTANCE = make();
    
    public static TrainIsNotOnThisNodeQuerySpecification make() {
      return new TrainIsNotOnThisNodeQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static TrainIsNotOnThisNodeQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.trainIsNotOnThisNode";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("node");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("node", "railway.Node"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_node = body.getOrCreateVariableByName("node");
      	PVariable var__train = body.getOrCreateVariableByName("_train");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_node, "node")
      	));
      	new TypeUnary(body, var_node, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Node"), "hu.bme.mit.mdsd.railway/Node");
      	new NegativePatternCall(body, new FlatTuple(var_node, var__train), FindTrainsOnNodeQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
