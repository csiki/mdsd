package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.IsDangerouslyConnectedMatch;
import hu.bme.mit.mdsd.railway.util.IsDangerouslyConnectedQuerySpecification;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import org.eclipse.incquery.runtime.api.IQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseMatcher;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.tuple.Tuple;
import org.eclipse.incquery.runtime.util.IncQueryLoggingUtil;
import railway.Semaphore;

/**
 * Generated pattern matcher API of the hu.bme.mit.mdsd.railway.isDangerouslyConnected pattern,
 * providing pattern-specific query methods.
 * 
 * <p>Use the pattern matcher on a given model via {@link #on(IncQueryEngine)},
 * e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}.
 * 
 * <p>Matches of the pattern will be represented as {@link IsDangerouslyConnectedMatch}.
 * 
 * <p>Original source:
 * <code><pre>
 * // nincs �letvesz�lyesen elhelyezett jelz�l�mpa
 * {@literal @}Constraint(severity="error",
 * 	message="Semaphores should facing in one direction or facing to each other.",
 * 	location=s1
 * )
 * 
 * pattern isDangerouslyConnected(s1 : Semaphore, s2 : Semaphore) {
 * 	s1 != s2;
 * 	Semaphore.connectsTo(s1, e1);
 * 	Semaphore.connectsTo(s1, e2);
 * 	e1 != e2;
 * 	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(e1, e2); 	// a k�t szemafor k�z�tt van szemafor n�lk�li, j� v�lt��ll�s� �tvonal
 * 	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
 * } or {
 * 	s1 != s2;
 * 	Semaphore.connectsTo(s1, e1);
 * 	Semaphore.connectsTo(s1, e2);
 * 	e1 == e2;
 * 	neg find isSwitch(e1);
 * 	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(e1, e2); 	// a k�t szemafor k�z�tt van szemafor n�lk�li, j� v�lt��ll�s� �tvonal
 * 	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
 * } or {
 * 	s1 != s2;
 * 	Semaphore.connectsTo(s1, e1);
 * 	Semaphore.connectsTo(s1, e2);
 * 	e1 == e2;
 * 	Switch(e1);
 * 	Switch.connections(e1, c1);
 * 	SwitchPos.from(c1, s1);
 * 	SwitchPos.to(c1, s2);
 * 	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
 * } or {
 * 	s1 != s2;
 * 	Semaphore.connectsTo(s1, e1);
 * 	Semaphore.connectsTo(s1, e2);
 * 	e1 == e2;
 * 	Switch(e1);
 * 	Switch.connections(e1, c1);
 * 	SwitchPos.from(c1, s2);
 * 	SwitchPos.to(c1, s1);
 * 	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
 * }
 * </pre></code>
 * 
 * @see IsDangerouslyConnectedMatch
 * @see IsDangerouslyConnectedProcessor
 * @see IsDangerouslyConnectedQuerySpecification
 * 
 */
@SuppressWarnings("all")
public class IsDangerouslyConnectedMatcher extends BaseMatcher<IsDangerouslyConnectedMatch> {
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * 
   */
  public static IsDangerouslyConnectedMatcher on(final IncQueryEngine engine) throws IncQueryException {
    // check if matcher already exists
    IsDangerouslyConnectedMatcher matcher = engine.getExistingMatcher(querySpecification());
    if (matcher == null) {
    	matcher = new IsDangerouslyConnectedMatcher(engine);
    	// do not have to "put" it into engine.matchers, reportMatcherInitialized() will take care of it
    }
    return matcher;
  }
  
  private final static int POSITION_S1 = 0;
  
  private final static int POSITION_S2 = 1;
  
  private final static Logger LOGGER = IncQueryLoggingUtil.getLogger(IsDangerouslyConnectedMatcher.class);
  
  /**
   * Initializes the pattern matcher over a given EMF model root (recommended: Resource or ResourceSet).
   * If a pattern matcher is already constructed with the same root, only a light-weight reference is returned.
   * The scope of pattern matching will be the given EMF model root and below (see FAQ for more precise definition).
   * The match set will be incrementally refreshed upon updates from this scope.
   * <p>The matcher will be created within the managed {@link IncQueryEngine} belonging to the EMF model root, so
   * multiple matchers will reuse the same engine and benefit from increased performance and reduced memory footprint.
   * @param emfRoot the root of the EMF containment hierarchy where the pattern matcher will operate. Recommended: Resource or ResourceSet.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead, e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}
   * 
   */
  @Deprecated
  public IsDangerouslyConnectedMatcher(final Notifier emfRoot) throws IncQueryException {
    this(IncQueryEngine.on(emfRoot));
  }
  
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead
   * 
   */
  @Deprecated
  public IsDangerouslyConnectedMatcher(final IncQueryEngine engine) throws IncQueryException {
    super(engine, querySpecification());
  }
  
  /**
   * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pS1 the fixed value of pattern parameter s1, or null if not bound.
   * @param pS2 the fixed value of pattern parameter s2, or null if not bound.
   * @return matches represented as a IsDangerouslyConnectedMatch object.
   * 
   */
  public Collection<IsDangerouslyConnectedMatch> getAllMatches(final Semaphore pS1, final Semaphore pS2) {
    return rawGetAllMatches(new Object[]{pS1, pS2});
  }
  
  /**
   * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pS1 the fixed value of pattern parameter s1, or null if not bound.
   * @param pS2 the fixed value of pattern parameter s2, or null if not bound.
   * @return a match represented as a IsDangerouslyConnectedMatch object, or null if no match is found.
   * 
   */
  public IsDangerouslyConnectedMatch getOneArbitraryMatch(final Semaphore pS1, final Semaphore pS2) {
    return rawGetOneArbitraryMatch(new Object[]{pS1, pS2});
  }
  
  /**
   * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
   * under any possible substitution of the unspecified parameters (if any).
   * @param pS1 the fixed value of pattern parameter s1, or null if not bound.
   * @param pS2 the fixed value of pattern parameter s2, or null if not bound.
   * @return true if the input is a valid (partial) match of the pattern.
   * 
   */
  public boolean hasMatch(final Semaphore pS1, final Semaphore pS2) {
    return rawHasMatch(new Object[]{pS1, pS2});
  }
  
  /**
   * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pS1 the fixed value of pattern parameter s1, or null if not bound.
   * @param pS2 the fixed value of pattern parameter s2, or null if not bound.
   * @return the number of pattern matches found.
   * 
   */
  public int countMatches(final Semaphore pS1, final Semaphore pS2) {
    return rawCountMatches(new Object[]{pS1, pS2});
  }
  
  /**
   * Executes the given processor on each match of the pattern that conforms to the given fixed values of some parameters.
   * @param pS1 the fixed value of pattern parameter s1, or null if not bound.
   * @param pS2 the fixed value of pattern parameter s2, or null if not bound.
   * @param processor the action that will process each pattern match.
   * 
   */
  public void forEachMatch(final Semaphore pS1, final Semaphore pS2, final IMatchProcessor<? super IsDangerouslyConnectedMatch> processor) {
    rawForEachMatch(new Object[]{pS1, pS2}, processor);
  }
  
  /**
   * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pS1 the fixed value of pattern parameter s1, or null if not bound.
   * @param pS2 the fixed value of pattern parameter s2, or null if not bound.
   * @param processor the action that will process the selected match.
   * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
   * 
   */
  public boolean forOneArbitraryMatch(final Semaphore pS1, final Semaphore pS2, final IMatchProcessor<? super IsDangerouslyConnectedMatch> processor) {
    return rawForOneArbitraryMatch(new Object[]{pS1, pS2}, processor);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pS1 the fixed value of pattern parameter s1, or null if not bound.
   * @param pS2 the fixed value of pattern parameter s2, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public IsDangerouslyConnectedMatch newMatch(final Semaphore pS1, final Semaphore pS2) {
    return IsDangerouslyConnectedMatch.newMatch(pS1, pS2);
  }
  
  /**
   * Retrieve the set of values that occur in matches for s1.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  protected Set<Semaphore> rawAccumulateAllValuesOfs1(final Object[] parameters) {
    Set<Semaphore> results = new HashSet<Semaphore>();
    rawAccumulateAllValues(POSITION_S1, parameters, results);
    return results;
  }
  
  /**
   * Retrieve the set of values that occur in matches for s1.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Semaphore> getAllValuesOfs1() {
    return rawAccumulateAllValuesOfs1(emptyArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for s1.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Semaphore> getAllValuesOfs1(final IsDangerouslyConnectedMatch partialMatch) {
    return rawAccumulateAllValuesOfs1(partialMatch.toArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for s1.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Semaphore> getAllValuesOfs1(final Semaphore pS2) {
    return rawAccumulateAllValuesOfs1(new Object[]{
    null, 
    pS2
    });
  }
  
  /**
   * Retrieve the set of values that occur in matches for s2.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  protected Set<Semaphore> rawAccumulateAllValuesOfs2(final Object[] parameters) {
    Set<Semaphore> results = new HashSet<Semaphore>();
    rawAccumulateAllValues(POSITION_S2, parameters, results);
    return results;
  }
  
  /**
   * Retrieve the set of values that occur in matches for s2.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Semaphore> getAllValuesOfs2() {
    return rawAccumulateAllValuesOfs2(emptyArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for s2.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Semaphore> getAllValuesOfs2(final IsDangerouslyConnectedMatch partialMatch) {
    return rawAccumulateAllValuesOfs2(partialMatch.toArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for s2.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Semaphore> getAllValuesOfs2(final Semaphore pS1) {
    return rawAccumulateAllValuesOfs2(new Object[]{
    pS1, 
    null
    });
  }
  
  @Override
  protected IsDangerouslyConnectedMatch tupleToMatch(final Tuple t) {
    try {
    	return IsDangerouslyConnectedMatch.newMatch((railway.Semaphore) t.get(POSITION_S1), (railway.Semaphore) t.get(POSITION_S2));
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in tuple not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected IsDangerouslyConnectedMatch arrayToMatch(final Object[] match) {
    try {
    	return IsDangerouslyConnectedMatch.newMatch((railway.Semaphore) match[POSITION_S1], (railway.Semaphore) match[POSITION_S2]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected IsDangerouslyConnectedMatch arrayToMatchMutable(final Object[] match) {
    try {
    	return IsDangerouslyConnectedMatch.newMutableMatch((railway.Semaphore) match[POSITION_S1], (railway.Semaphore) match[POSITION_S2]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  /**
   * @return the singleton instance of the query specification of this pattern
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IQuerySpecification<IsDangerouslyConnectedMatcher> querySpecification() throws IncQueryException {
    return IsDangerouslyConnectedQuerySpecification.instance();
  }
}
