package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.IsEveryTrainPathValidMatch;
import hu.bme.mit.mdsd.railway.IsEveryTrainPathValidMatcher;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.BinaryTransitiveClosure;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate IsEveryTrainPathValidMatcher in a type-safe way.
 * 
 * @see IsEveryTrainPathValidMatcher
 * @see IsEveryTrainPathValidMatch
 * 
 */
@SuppressWarnings("all")
public final class IsEveryTrainPathValidQuerySpecification extends BaseGeneratedEMFQuerySpecification<IsEveryTrainPathValidMatcher> {
  private IsEveryTrainPathValidQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IsEveryTrainPathValidQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected IsEveryTrainPathValidMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return IsEveryTrainPathValidMatcher.on(engine);
  }
  
  @Override
  public IsEveryTrainPathValidMatch newEmptyMatch() {
    return IsEveryTrainPathValidMatch.newEmptyMatch();
  }
  
  @Override
  public IsEveryTrainPathValidMatch newMatch(final Object... parameters) {
    return IsEveryTrainPathValidMatch.newMatch();
  }
  
  private static class LazyHolder {
    private final static IsEveryTrainPathValidQuerySpecification INSTANCE = make();
    
    public static IsEveryTrainPathValidQuerySpecification make() {
      return new IsEveryTrainPathValidQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static IsEveryTrainPathValidQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.isEveryTrainPathValid";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList();
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList();
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_t = body.getOrCreateVariableByName("t");
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      	));
      	new TypeUnary(body, var_e1, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	new TypeUnary(body, var_e2, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	new Inequality(body, var_e1, var_e2);
      	new TypeBinary(body, CONTEXT, var_t, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	new TypeBinary(body, CONTEXT, var_t, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	new BinaryTransitiveClosure(body, new FlatTuple(var_e1, var_e2), DirectlyConnectedQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
