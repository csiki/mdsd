package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.DirectlyConnectedButNotSemaphoreMatcher;
import hu.bme.mit.mdsd.railway.DirectlyConnectedMatcher;
import hu.bme.mit.mdsd.railway.FindTrainsOnNodeMatcher;
import hu.bme.mit.mdsd.railway.GetAllTrainPairsOnSempahorelessPathMatcher;
import hu.bme.mit.mdsd.railway.IsDangerouslyConnectedMatcher;
import hu.bme.mit.mdsd.railway.IsSemaphoreMatcher;
import hu.bme.mit.mdsd.railway.NoDangerousSempahorePairsMatcher;
import hu.bme.mit.mdsd.railway.NoTrainPairsOnSempahorelessPathMatcher;
import hu.bme.mit.mdsd.railway.TrainIsNotOnThisNodeMatcher;
import hu.bme.mit.mdsd.railway.TrainsHaveTwoDeadendsInPathMatcher;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedButNotSemaphoreQuerySpecification;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedQuerySpecification;
import hu.bme.mit.mdsd.railway.util.FindTrainsOnNodeQuerySpecification;
import hu.bme.mit.mdsd.railway.util.GetAllTrainPairsOnSempahorelessPathQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsDangerouslyConnectedQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsSemaphoreQuerySpecification;
import hu.bme.mit.mdsd.railway.util.NoDangerousSempahorePairsQuerySpecification;
import hu.bme.mit.mdsd.railway.util.NoTrainPairsOnSempahorelessPathQuerySpecification;
import hu.bme.mit.mdsd.railway.util.TrainIsNotOnThisNodeQuerySpecification;
import hu.bme.mit.mdsd.railway.util.TrainsHaveTwoDeadendsInPathQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedPatternGroup;
import org.eclipse.incquery.runtime.exception.IncQueryException;

/**
 * A pattern group formed of all patterns defined in queries.eiq.
 * 
 * <p>Use the static instance as any {@link org.eclipse.incquery.runtime.api.IPatternGroup}, to conveniently prepare
 * an EMF-IncQuery engine for matching all patterns originally defined in file queries.eiq,
 * in order to achieve better performance than one-by-one on-demand matcher initialization.
 * 
 * <p> From package hu.bme.mit.mdsd.railway, the group contains the definition of the following patterns: <ul>
 * <li>isSemaphore</li>
 * <li>trainIsNotOnThisNode</li>
 * <li>findTrainsOnNode</li>
 * <li>noTrainPairsOnSempahorelessPath</li>
 * <li>getAllTrainPairsOnSempahorelessPath</li>
 * <li>directlyConnectedButNotSemaphore</li>
 * <li>noDangerousSempahorePairs</li>
 * <li>directlyConnected</li>
 * <li>isDangerouslyConnected</li>
 * <li>trainsHaveTwoDeadendsInPath</li>
 * </ul>
 * 
 * @see IPatternGroup
 * 
 */
@SuppressWarnings("all")
public final class Queries extends BaseGeneratedPatternGroup {
  /**
   * Access the pattern group.
   * 
   * @return the singleton instance of the group
   * @throws IncQueryException if there was an error loading the generated code of pattern specifications
   * 
   */
  public static Queries instance() throws IncQueryException {
    if (INSTANCE == null) {
    	INSTANCE = new Queries();
    }
    return INSTANCE;
  }
  
  private static Queries INSTANCE;
  
  private Queries() throws IncQueryException {
    querySpecifications.add(IsSemaphoreQuerySpecification.instance());
    querySpecifications.add(TrainIsNotOnThisNodeQuerySpecification.instance());
    querySpecifications.add(FindTrainsOnNodeQuerySpecification.instance());
    querySpecifications.add(NoTrainPairsOnSempahorelessPathQuerySpecification.instance());
    querySpecifications.add(GetAllTrainPairsOnSempahorelessPathQuerySpecification.instance());
    querySpecifications.add(DirectlyConnectedButNotSemaphoreQuerySpecification.instance());
    querySpecifications.add(NoDangerousSempahorePairsQuerySpecification.instance());
    querySpecifications.add(DirectlyConnectedQuerySpecification.instance());
    querySpecifications.add(IsDangerouslyConnectedQuerySpecification.instance());
    querySpecifications.add(TrainsHaveTwoDeadendsInPathQuerySpecification.instance());
  }
  
  public IsSemaphoreQuerySpecification getIsSemaphore() throws IncQueryException {
    return IsSemaphoreQuerySpecification.instance();
  }
  
  public IsSemaphoreMatcher getIsSemaphore(final IncQueryEngine engine) throws IncQueryException {
    return IsSemaphoreMatcher.on(engine);
  }
  
  public TrainIsNotOnThisNodeQuerySpecification getTrainIsNotOnThisNode() throws IncQueryException {
    return TrainIsNotOnThisNodeQuerySpecification.instance();
  }
  
  public TrainIsNotOnThisNodeMatcher getTrainIsNotOnThisNode(final IncQueryEngine engine) throws IncQueryException {
    return TrainIsNotOnThisNodeMatcher.on(engine);
  }
  
  public FindTrainsOnNodeQuerySpecification getFindTrainsOnNode() throws IncQueryException {
    return FindTrainsOnNodeQuerySpecification.instance();
  }
  
  public FindTrainsOnNodeMatcher getFindTrainsOnNode(final IncQueryEngine engine) throws IncQueryException {
    return FindTrainsOnNodeMatcher.on(engine);
  }
  
  public NoTrainPairsOnSempahorelessPathQuerySpecification getNoTrainPairsOnSempahorelessPath() throws IncQueryException {
    return NoTrainPairsOnSempahorelessPathQuerySpecification.instance();
  }
  
  public NoTrainPairsOnSempahorelessPathMatcher getNoTrainPairsOnSempahorelessPath(final IncQueryEngine engine) throws IncQueryException {
    return NoTrainPairsOnSempahorelessPathMatcher.on(engine);
  }
  
  public GetAllTrainPairsOnSempahorelessPathQuerySpecification getGetAllTrainPairsOnSempahorelessPath() throws IncQueryException {
    return GetAllTrainPairsOnSempahorelessPathQuerySpecification.instance();
  }
  
  public GetAllTrainPairsOnSempahorelessPathMatcher getGetAllTrainPairsOnSempahorelessPath(final IncQueryEngine engine) throws IncQueryException {
    return GetAllTrainPairsOnSempahorelessPathMatcher.on(engine);
  }
  
  public DirectlyConnectedButNotSemaphoreQuerySpecification getDirectlyConnectedButNotSemaphore() throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreQuerySpecification.instance();
  }
  
  public DirectlyConnectedButNotSemaphoreMatcher getDirectlyConnectedButNotSemaphore(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreMatcher.on(engine);
  }
  
  public NoDangerousSempahorePairsQuerySpecification getNoDangerousSempahorePairs() throws IncQueryException {
    return NoDangerousSempahorePairsQuerySpecification.instance();
  }
  
  public NoDangerousSempahorePairsMatcher getNoDangerousSempahorePairs(final IncQueryEngine engine) throws IncQueryException {
    return NoDangerousSempahorePairsMatcher.on(engine);
  }
  
  public DirectlyConnectedQuerySpecification getDirectlyConnected() throws IncQueryException {
    return DirectlyConnectedQuerySpecification.instance();
  }
  
  public DirectlyConnectedMatcher getDirectlyConnected(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedMatcher.on(engine);
  }
  
  public IsDangerouslyConnectedQuerySpecification getIsDangerouslyConnected() throws IncQueryException {
    return IsDangerouslyConnectedQuerySpecification.instance();
  }
  
  public IsDangerouslyConnectedMatcher getIsDangerouslyConnected(final IncQueryEngine engine) throws IncQueryException {
    return IsDangerouslyConnectedMatcher.on(engine);
  }
  
  public TrainsHaveTwoDeadendsInPathQuerySpecification getTrainsHaveTwoDeadendsInPath() throws IncQueryException {
    return TrainsHaveTwoDeadendsInPathQuerySpecification.instance();
  }
  
  public TrainsHaveTwoDeadendsInPathMatcher getTrainsHaveTwoDeadendsInPath(final IncQueryEngine engine) throws IncQueryException {
    return TrainsHaveTwoDeadendsInPathMatcher.on(engine);
  }
}
