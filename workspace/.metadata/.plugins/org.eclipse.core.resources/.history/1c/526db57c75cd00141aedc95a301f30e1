package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.GetAllSemaphorePairMatch;
import hu.bme.mit.mdsd.railway.GetAllSemaphorePairMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate GetAllSemaphorePairMatcher in a type-safe way.
 * 
 * @see GetAllSemaphorePairMatcher
 * @see GetAllSemaphorePairMatch
 * 
 */
@SuppressWarnings("all")
public final class GetAllSemaphorePairQuerySpecification extends BaseGeneratedEMFQuerySpecification<GetAllSemaphorePairMatcher> {
  private GetAllSemaphorePairQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static GetAllSemaphorePairQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected GetAllSemaphorePairMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return GetAllSemaphorePairMatcher.on(engine);
  }
  
  @Override
  public GetAllSemaphorePairMatch newEmptyMatch() {
    return GetAllSemaphorePairMatch.newEmptyMatch();
  }
  
  @Override
  public GetAllSemaphorePairMatch newMatch(final Object... parameters) {
    return GetAllSemaphorePairMatch.newMatch((railway.Semaphore) parameters[0], (railway.Semaphore) parameters[1]);
  }
  
  private static class LazyHolder {
    private final static GetAllSemaphorePairQuerySpecification INSTANCE = make();
    
    public static GetAllSemaphorePairQuerySpecification make() {
      return new GetAllSemaphorePairQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static GetAllSemaphorePairQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.getAllSemaphorePair";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("s1","s2");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("s1", "railway.Semaphore"),new PParameter("s2", "railway.Semaphore"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_s1 = body.getOrCreateVariableByName("s1");
      	PVariable var_s2 = body.getOrCreateVariableByName("s2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_s1, "s1"),
      				
      		new ExportedParameter(body, var_s2, "s2")
      	));
      	new TypeUnary(body, var_s1, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Semaphore"), "hu.bme.mit.mdsd.railway/Semaphore");
      	new TypeUnary(body, var_s2, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Semaphore"), "hu.bme.mit.mdsd.railway/Semaphore");
      	new Inequality(body, var_s1, var_s2);
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
