package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.util.IsMoreSemaphoreThanTrainQuerySpecification;
import java.util.Arrays;
import java.util.List;
import org.eclipse.incquery.runtime.api.IPatternMatch;
import org.eclipse.incquery.runtime.api.impl.BasePatternMatch;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import railway.MAV;

/**
 * Pattern-specific match representation of the hu.bme.mit.mdsd.railway.isMoreSemaphoreThanTrain pattern,
 * to be used in conjunction with {@link IsMoreSemaphoreThanTrainMatcher}.
 * 
 * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
 * Each instance is a (possibly partial) substitution of pattern parameters,
 * usable to represent a match of the pattern in the result of a query,
 * or to specify the bound (fixed) input parameters when issuing a query.
 * 
 * @see IsMoreSemaphoreThanTrainMatcher
 * @see IsMoreSemaphoreThanTrainProcessor
 * 
 */
@SuppressWarnings("all")
public abstract class IsMoreSemaphoreThanTrainMatch extends BasePatternMatch {
  private MAV fMav;
  
  private static List<String> parameterNames = makeImmutableList("mav");
  
  private IsMoreSemaphoreThanTrainMatch(final MAV pMav) {
    this.fMav = pMav;
  }
  
  @Override
  public Object get(final String parameterName) {
    if ("mav".equals(parameterName)) return this.fMav;
    return null;
  }
  
  public MAV getMav() {
    return this.fMav;
  }
  
  @Override
  public boolean set(final String parameterName, final Object newValue) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    if ("mav".equals(parameterName) ) {
    	this.fMav = (railway.MAV) newValue;
    	return true;
    }
    return false;
  }
  
  public void setMav(final MAV pMav) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    this.fMav = pMav;
  }
  
  @Override
  public String patternName() {
    return "hu.bme.mit.mdsd.railway.isMoreSemaphoreThanTrain";
  }
  
  @Override
  public List<String> parameterNames() {
    return IsMoreSemaphoreThanTrainMatch.parameterNames;
  }
  
  @Override
  public Object[] toArray() {
    return new Object[]{fMav};
  }
  
  @Override
  public IsMoreSemaphoreThanTrainMatch toImmutable() {
    return isMutable() ? newMatch(fMav) : this;
  }
  
  @Override
  public String prettyPrint() {
    StringBuilder result = new StringBuilder();
    result.append("\"mav\"=" + prettyPrintValue(fMav)
    );
    return result.toString();
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fMav == null) ? 0 : fMav.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
    	return true;
    if (!(obj instanceof IsMoreSemaphoreThanTrainMatch)) { // this should be infrequent
    	if (obj == null) {
    		return false;
    	}
    	if (!(obj instanceof IPatternMatch)) {
    		return false;
    	}
    	IPatternMatch otherSig  = (IPatternMatch) obj;
    	if (!specification().equals(otherSig.specification()))
    		return false;
    	return Arrays.deepEquals(toArray(), otherSig.toArray());
    }
    IsMoreSemaphoreThanTrainMatch other = (IsMoreSemaphoreThanTrainMatch) obj;
    if (fMav == null) {if (other.fMav != null) return false;}
    else if (!fMav.equals(other.fMav)) return false;
    return true;
  }
  
  @Override
  public IsMoreSemaphoreThanTrainQuerySpecification specification() {
    try {
    	return IsMoreSemaphoreThanTrainQuerySpecification.instance();
    } catch (IncQueryException ex) {
     	// This cannot happen, as the match object can only be instantiated if the query specification exists
     	throw new IllegalStateException (ex);
    }
  }
  
  /**
   * Returns an empty, mutable match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @return the empty match.
   * 
   */
  public static IsMoreSemaphoreThanTrainMatch newEmptyMatch() {
    return new Mutable(null);
  }
  
  /**
   * Returns a mutable (partial) match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @param pMav the fixed value of pattern parameter mav, or null if not bound.
   * @return the new, mutable (partial) match object.
   * 
   */
  public static IsMoreSemaphoreThanTrainMatch newMutableMatch(final MAV pMav) {
    return new Mutable(pMav);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pMav the fixed value of pattern parameter mav, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public static IsMoreSemaphoreThanTrainMatch newMatch(final MAV pMav) {
    return new Immutable(pMav);
  }
  
  private static final class Mutable extends IsMoreSemaphoreThanTrainMatch {
    Mutable(final MAV pMav) {
      super(pMav);
    }
    
    @Override
    public boolean isMutable() {
      return true;
    }
  }
  
  private static final class Immutable extends IsMoreSemaphoreThanTrainMatch {
    Immutable(final MAV pMav) {
      super(pMav);
    }
    
    @Override
    public boolean isMutable() {
      return false;
    }
  }
}
