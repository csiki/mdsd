package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.TrainIsNotOnThisNodeMatch;
import hu.bme.mit.mdsd.railway.util.TrainIsNotOnThisNodeQuerySpecification;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.incquery.runtime.api.IQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseMatcher;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.tuple.Tuple;
import org.eclipse.incquery.runtime.util.IncQueryLoggingUtil;

/**
 * Generated pattern matcher API of the hu.bme.mit.mdsd.railway.trainIsNotOnThisNode pattern,
 * providing pattern-specific query methods.
 * 
 * <p>Use the pattern matcher on a given model via {@link #on(IncQueryEngine)},
 * e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}.
 * 
 * <p>Matches of the pattern will be represented as {@link TrainIsNotOnThisNodeMatch}.
 * 
 * <p>Original source:
 * <code><pre>
 * // metamodellben nem kifejezhet� de �rtelmetlen esetek kisz�r�se:
 * // semmibe vezet� s�nszakasz -- tudja a modell?
 * 
 * // vonat csak sectionon lehet
 * 
 * pattern trainIsNotOnThisNode() {					// futtatni
 * 	Node(node);
 * 	neg find findTrainsOnNode(node, _train);
 * }
 * </pre></code>
 * 
 * @see TrainIsNotOnThisNodeMatch
 * @see TrainIsNotOnThisNodeProcessor
 * @see TrainIsNotOnThisNodeQuerySpecification
 * 
 */
@SuppressWarnings("all")
public class TrainIsNotOnThisNodeMatcher extends BaseMatcher<TrainIsNotOnThisNodeMatch> {
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * 
   */
  public static TrainIsNotOnThisNodeMatcher on(final IncQueryEngine engine) throws IncQueryException {
    // check if matcher already exists
    TrainIsNotOnThisNodeMatcher matcher = engine.getExistingMatcher(querySpecification());
    if (matcher == null) {
    	matcher = new TrainIsNotOnThisNodeMatcher(engine);
    	// do not have to "put" it into engine.matchers, reportMatcherInitialized() will take care of it
    }
    return matcher;
  }
  
  private final static Logger LOGGER = IncQueryLoggingUtil.getLogger(TrainIsNotOnThisNodeMatcher.class);
  
  /**
   * Initializes the pattern matcher over a given EMF model root (recommended: Resource or ResourceSet).
   * If a pattern matcher is already constructed with the same root, only a light-weight reference is returned.
   * The scope of pattern matching will be the given EMF model root and below (see FAQ for more precise definition).
   * The match set will be incrementally refreshed upon updates from this scope.
   * <p>The matcher will be created within the managed {@link IncQueryEngine} belonging to the EMF model root, so
   * multiple matchers will reuse the same engine and benefit from increased performance and reduced memory footprint.
   * @param emfRoot the root of the EMF containment hierarchy where the pattern matcher will operate. Recommended: Resource or ResourceSet.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead, e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}
   * 
   */
  @Deprecated
  public TrainIsNotOnThisNodeMatcher(final Notifier emfRoot) throws IncQueryException {
    this(IncQueryEngine.on(emfRoot));
  }
  
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead
   * 
   */
  @Deprecated
  public TrainIsNotOnThisNodeMatcher(final IncQueryEngine engine) throws IncQueryException {
    super(engine, querySpecification());
  }
  
  /**
   * Indicates whether the (parameterless) pattern matches or not.
   * @return true if the pattern has a valid match.
   * 
   */
  public boolean hasMatch() {
    return rawHasMatch(new Object[]{});
  }
  
  @Override
  protected TrainIsNotOnThisNodeMatch tupleToMatch(final Tuple t) {
    try {
    	return TrainIsNotOnThisNodeMatch.newMatch();
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in tuple not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected TrainIsNotOnThisNodeMatch arrayToMatch(final Object[] match) {
    try {
    	return TrainIsNotOnThisNodeMatch.newMatch();
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected TrainIsNotOnThisNodeMatch arrayToMatchMutable(final Object[] match) {
    try {
    	return TrainIsNotOnThisNodeMatch.newMutableMatch();
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  /**
   * @return the singleton instance of the query specification of this pattern
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IQuerySpecification<TrainIsNotOnThisNodeMatcher> querySpecification() throws IncQueryException {
    return TrainIsNotOnThisNodeQuerySpecification.instance();
  }
}
