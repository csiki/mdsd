package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.NoTrainPairsOnSempahorelessPathMatch;
import hu.bme.mit.mdsd.railway.NoTrainPairsOnSempahorelessPathMatcher;
import hu.bme.mit.mdsd.railway.util.GetAllTrainPairsOnSempahorelessPathQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate NoTrainPairsOnSempahorelessPathMatcher in a type-safe way.
 * 
 * @see NoTrainPairsOnSempahorelessPathMatcher
 * @see NoTrainPairsOnSempahorelessPathMatch
 * 
 */
@SuppressWarnings("all")
public final class NoTrainPairsOnSempahorelessPathQuerySpecification extends BaseGeneratedEMFQuerySpecification<NoTrainPairsOnSempahorelessPathMatcher> {
  private NoTrainPairsOnSempahorelessPathQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static NoTrainPairsOnSempahorelessPathQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected NoTrainPairsOnSempahorelessPathMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return NoTrainPairsOnSempahorelessPathMatcher.on(engine);
  }
  
  @Override
  public NoTrainPairsOnSempahorelessPathMatch newEmptyMatch() {
    return NoTrainPairsOnSempahorelessPathMatch.newEmptyMatch();
  }
  
  @Override
  public NoTrainPairsOnSempahorelessPathMatch newMatch(final Object... parameters) {
    return NoTrainPairsOnSempahorelessPathMatch.newMatch();
  }
  
  private static class LazyHolder {
    private final static NoTrainPairsOnSempahorelessPathQuerySpecification INSTANCE = make();
    
    public static NoTrainPairsOnSempahorelessPathQuerySpecification make() {
      return new NoTrainPairsOnSempahorelessPathQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static NoTrainPairsOnSempahorelessPathQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.noTrainPairsOnSempahorelessPath";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList();
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList();
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_t1 = body.getOrCreateVariableByName("t1");
      	PVariable var_t2 = body.getOrCreateVariableByName("t2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      	));
      	new TypeUnary(body, var_t1, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Train"), "hu.bme.mit.mdsd.railway/Train");
      	new TypeUnary(body, var_t2, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Train"), "hu.bme.mit.mdsd.railway/Train");
      	new Inequality(body, var_t1, var_t2);
      	new NegativePatternCall(body, new FlatTuple(var_t1, var_t2), GetAllTrainPairsOnSempahorelessPathQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
