package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.DirectlyConnectedMatch;
import hu.bme.mit.mdsd.railway.DirectlyConnectedMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate DirectlyConnectedMatcher in a type-safe way.
 * 
 * @see DirectlyConnectedMatcher
 * @see DirectlyConnectedMatch
 * 
 */
@SuppressWarnings("all")
public final class DirectlyConnectedQuerySpecification extends BaseGeneratedEMFQuerySpecification<DirectlyConnectedMatcher> {
  private DirectlyConnectedQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static DirectlyConnectedQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected DirectlyConnectedMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedMatcher.on(engine);
  }
  
  @Override
  public DirectlyConnectedMatch newEmptyMatch() {
    return DirectlyConnectedMatch.newEmptyMatch();
  }
  
  @Override
  public DirectlyConnectedMatch newMatch(final Object... parameters) {
    return DirectlyConnectedMatch.newMatch((railway.TrackElement) parameters[0], (railway.TrackElement) parameters[1]);
  }
  
  private static class LazyHolder {
    private final static DirectlyConnectedQuerySpecification INSTANCE = make();
    
    public static DirectlyConnectedQuerySpecification make() {
      return new DirectlyConnectedQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static DirectlyConnectedQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.directlyConnected";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("e1","e2");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("e1", "railway.TrackElement"),new PParameter("e2", "railway.TrackElement"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_e1, "e1"),
      				
      		new ExportedParameter(body, var_e2, "e2")
      	));
      	new TypeBinary(body, CONTEXT, var_e1, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "connectsTo"), "hu.bme.mit.mdsd.railway/TrackElement.connectsTo");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
