package hu.bme.mit.mdsd.railway

import "hu.bme.mit.mdsd.railway"

// seg�df�ggv�nyek
pattern isSemaphore(e : TrackElement) {
	Semaphore(e);
}

pattern isSwitch(e : TrackElement) {
	Switch(e);
}

pattern countSemaphore() {
	Semaphore(_e);
}

pattern countTrain() {
	Train(_e);
}

pattern directlyConnected(e1 : TrackElement, e2 : TrackElement) {
	TrackElement.connectsTo(e1, e2);
}

pattern directlyConnectedButNotSemaphore(e1 : TrackElement, e2 : TrackElement) {
	TrackElement.connectsTo(e1, e2);
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
}

pattern directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected(e1 : TrackElement, e2 : TrackElement) {
	// a k�t elem azonos = a k�t elem k�z�tt csak egy van, �s mindkett� szomsz�os vele 
	// megjegyz�s: ez igaz akkor is, ha switch, de �ppen nincs �sszek�ttet�s a k�t k�rt szomsz�d k�z�tt, csup�n egy switch van k�z�tt�k
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	e1 == e2;
} or {
	// az els� switch, a m�sodik section, �s a switch fromja a section
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e1);
	neg find isSwitch(e2);
	Switch.connections(e1, c1);
	SwitchPos.from(c1, e2);
} or {
	// az els� switch, a m�sodik section, �s a switch toja a section
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e1);
	neg find isSwitch(e2);
	Switch.connections(e1, c1);
	SwitchPos.to(c1, e2);
} or {
	// az els� section, a m�sodik switch, �s a switch fromja a section
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e2);
	neg find isSwitch(e1);
	Switch.connections(e2, c1);
	SwitchPos.from(c1, e1);
} or {
	// az els� section, a m�sodik switch, �s a switch toja a section
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e2);
	neg find isSwitch(e1);
	Switch.connections(e2, c1);
	SwitchPos.to(c1, e1);
} or {
	// mindkett� switch, �s az els� a m�sodik fromj�ban van
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e2);
	find isSwitch(e1);
	Switch.connections(e2, c1);
	SwitchPos.from(c1, e1);
} or {
	// mindkett� switch, �s az els� a m�sodik toj�ban van
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e2);
	find isSwitch(e1);
	Switch.connections(e2, c1);
	SwitchPos.to(c1, e1);
} or {
	// mindkett� switch, �s a m�sodik az els� fromj�ban van
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e2);
	find isSwitch(e1);
	Switch.connections(e1, c1);
	SwitchPos.from(c1, e2);
}or {
	// mindkett� switch, �s a m�sodik az els� toj�ban van
	neg find isSemaphore(e1);
	neg find isSemaphore(e2);
	find isSwitch(e2);
	find isSwitch(e1);
	Switch.connections(e1, c1);
	SwitchPos.to(c1, e2);
}

// metamodellben nem kifejezhet� de �rtelmetlen esetek kisz�r�se:
// semmibe vezet� s�nszakasz -- tudja a modell?

// vonat csak sectionon lehet

@Constraint(severity="error",
	message="Train cannot be on a node!",
	location=node,
	targetEditorId = "railway.presentation.RailwayEditorID"
)
pattern findTrainsOnNode(node : Node, train : Train) {
	Node.train(node, train);
}

// k�t jelz�l�ma (L�MA!!!4!) k�z�tt csak egy vonat


/*pattern noTrainPairsOnSempahorelessPath() {	 		// futtatni
	Train(t1);
	Train(t2);
	t1 != t2;
	neg find getAllTrainPairsOnSempahorelessPath(t1, t2);
}*/

@Constraint(severity="error",
	message="Train cannot be on a node!",
	location=e1
)
pattern getAllTrainPairsOnSempahorelessPath(t1 : Train, t2 : Train, e1 : TrackElement, e2 : TrackElement) {	 		// futtatni
	t1 != t2;
	e1 != e2;
	//TrackElement(e1);
	TrackElement.train(e1, t1);
	//TrackElement(e2);
	TrackElement.train(e2, t2);
	find directlyConnectedButNotSemaphore+(e1, e2);
}

// nincs �letvesz�lyesen elhelyezett jelz�l�mpa
@Constraint(severity="error",
	message="Semaphores should facing in one direction or facing to each other.",
	location=s1
)

pattern isDangerouslyConnected(s1 : Semaphore, s2 : Semaphore) {
	s1 != s2;
	Semaphore.connectsTo(s1, e1);
	Semaphore.connectsTo(s1, e2);
	e1 != e2;
	neg find isSwitch(e1);
	neg find isSwitch(e2);
	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(e1, e2); 	// a k�t szemafor k�z�tt van szemafor n�lk�li, j� v�lt��ll�s� �tvonal
	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
} or {
	s1 != s2;
	Semaphore.connectsTo(s1, e1);
	Semaphore.connectsTo(s1, e2);
	e1 != e2;
	Switch(e1);
	neg find isSwitch(e2);
	Switch.connections(e1, c1);
	SwitchPos.from(c1, s1);
	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(e1, e2); 	// a k�t szemafor k�z�tt van szemafor n�lk�li, j� v�lt��ll�s� �tvonal
	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
} or {
	s1 != s2;
	Semaphore.connectsTo(s1, e1);
	Semaphore.connectsTo(s1, e2);
	e1 != e2;
	Switch(e1);
	neg find isSwitch(e2);
	Switch.connections(e1, c1);
	SwitchPos.to(c1, s1);
	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(e1, e2); 	// a k�t szemafor k�z�tt van szemafor n�lk�li, j� v�lt��ll�s� �tvonal
	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
} or {
	s1 != s2;
	Semaphore.connectsTo(s1, e1);
	Semaphore.connectsTo(s1, e2);
	e1 != e2;
	Switch(e1);
	neg find isSwitch(e2);
	Switch.connections(e1, c1);
	SwitchPos.from(c1, s1);
	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(e1, e2); 	// a k�t szemafor k�z�tt van szemafor n�lk�li, j� v�lt��ll�s� �tvonal
	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
} or {
	s1 != s2;
	Semaphore.connectsTo(s1, e1);
	Semaphore.connectsTo(s1, e2);
	e1 == e2;
	neg find isSwitch(e1);
	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(e1, e2); 	// a k�t szemafor k�z�tt van szemafor n�lk�li, j� v�lt��ll�s� �tvonal
	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
} or {
	s1 != s2;
	Semaphore.connectsTo(s1, e1);
	Semaphore.connectsTo(s1, e2);
	e1 == e2;
	Switch(e1);
	Switch.connections(e1, c1);														// A k�zt�k l�v� elem switch, �s az �sszek�ti �ket t�nylegesen az adott pillanatban
	SwitchPos.from(c1, s1);
	SwitchPos.to(c1, s2);
	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
} or {
	s1 != s2;
	Semaphore.connectsTo(s1, e1);
	Semaphore.connectsTo(s1, e2);
	e1 == e2;
	Switch(e1);
	Switch.connections(e1, c1);														// A k�zt�k l�v� elem switch, �s az �sszek�ti �ket t�nylegesen az adott pillanatban
	SwitchPos.from(c1, s2);
	SwitchPos.to(c1, s1);
	neg find isGoodConnected(s1, s2);												// de nincs "helyes" �tvonal
}

pattern isGoodConnected(s1 : Semaphore, s2 : Semaphore) {
	s1 != s2;
	
	Semaphore.elements(s1, e1);
	Semaphore.facing(s1, f1);
	e1 != f1;
	
	Semaphore.elements(s2, e2);
	Semaphore.facing(s2, f2);
	e2 != f2;
	
	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(f1, e2);
} or {
	s1 != s2;
	
	Semaphore.elements(s1, e1);
	Semaphore.facing(s1, f1);
	e1 != f1;
	
	Semaphore.elements(s2, e2);
	Semaphore.facing(s2, f2);
	e2 != f2;
	
	find directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected+(f2, e1);
} or {
	s1 != s2;
	
	Semaphore.elements(s1, e1);
	Semaphore.facing(s1, f1);
	e1 != f1;
	
	Semaphore.elements(s2, e2);
	Semaphore.facing(s2, f2);
	e2 != f2;
	
	f1 == s2;
	f2 == s1;
}

/*pattern isDangerouslyConnected(s1 : Semaphore, s2 : Semaphore) {
	s1 != s2;
	
	Semaphore.elements(s1, e1);
	Semaphore.facing(s1, f1);
	e1 != f1;
	f1 != s2;										// ha egym�s fel� �llnak, nem lehetnek szomsz�dosak
	
	Semaphore.elements(s2, e2);
	Semaphore.facing(s2, f2);
	e2 != f2;
	f2 != s1;										// ha egym�s fel� �llnak, nem lehetnek szomsz�dosak
	
	find directlyConnected+(e1, e2);				// van �t a facinggel ellent�tes oldalon l�v� elemek k�z�tt, midnegy melyik ir�nyba (szemaforokon kereszt�l vagy sem), mindenk�ppen vesz�lyes �tszakasz
}*/

// vonat ne menjen olyan gyorsan, hogy ne tudjon id�ben meg�llni a megfelel� jelz�sek ellen�re sem (pl. k�vetkez� semafor piros jelz�s eset�n)
	// ==> viselked�si modell

// ne legyen olyan �llapot, hogy egyik vonat sem tud tov�bbmenni (holtpont)
	// ==> k�sz a fenti noTrainPairsOnSempahorelessPath �s noDangerousSempahorePairs patternnel
	// + t�bb l�mpa legyen mint vonat

@Constraint(severity="error",
	message="Should be more semaphores on track than trains! Except when there's only one train.",
	location=mav
)
pattern isMoreSemaphoreThanTrain(mav : MAV) {				// futtat�s
	MAV(mav);
	n == count find countSemaphore();
	m == count find countTrain();
	check(n <= m);	// nicns t�bb szemafor mint vonat
	check(m > 1);	// nem egy vagy nulla vonat van
}

// vonat mindig dead-endb�l dead-endbe menjen = pontosan k�t dead-endet tartalmaz

@Constraint(severity="error",
	message="Route of the train should contain at least two dead-ends!",
	location=t
)
pattern trainsHaveExactlyTwoDeadendsInPath(t : Train) {	 		// futtatni
	//Train(t);
	neg find isAtLeastTwoDeadEndInTrainPath(t);
} or {
	find isAtLeastThreeDeadEndInTrainPath(t);
}

pattern isAtLeastThreeDeadEndInTrainPath(t : Train) {
	Deadend(e1);
	Deadend(e2);
	Deadend(e3);
	e1 != e2;
	e1 != e3;
	e2 != e3;
	Train.route(t, e1); 
	Train.route(t, e2); 
	Train.route(t, e3); 
}

pattern isAtLeastTwoDeadEndInTrainPath(t : Train) {
	Deadend(e1);
	Deadend(e2);
	e1 != e2;
	Train.route(t, e1); 
	Train.route(t, e2);  
}

// a k�t dead-end k�z�tt legyen lehets�ges �sszek�ttet�s

@Constraint(severity="error",
	message="There should be at least one route between two dead-ends (start and end point of train route)!",
	location=t
)

pattern notEveryTrainPathValid(t : Train) {
	neg find isEveryTrainPathValid(t);
}

pattern isEveryTrainPathValid(t : Train) {	 		// futtatni
	Deadend(e1);
	Deadend(e2);
	e1 != e2;
	Train.route(t, e1);
	Train.route(t, e2);
	find directlyConnected+(e1, e2);
}

// felt�telez�sek: 
// a connectsTo �s a trackElementb�l lesz�rmaz�k elements-jei ugyan azok (redund�nsak, de azonosokat)
// node-ok pontszer�ek
// vonatok ismerik a p�lyarendszert (milyen elem hol van, milyen tulajdons�gaik vannak)
// egy vonat lass�l�sa �s gyorsul�sa azonos m�rt�k�
// s�nt �s fels�vezet�ket nem lopnak
// vonat nem robban le
// vonat k�vetkezetesen viselkedik (k�veti az �tvonal�t)
// dead-end "sz�li" �s "eszi meg" a vonatokat
// t�bb a szemafor mint a vonat