package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.IsDeadendMatch;
import hu.bme.mit.mdsd.railway.IsDeadendMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate IsDeadendMatcher in a type-safe way.
 * 
 * @see IsDeadendMatcher
 * @see IsDeadendMatch
 * 
 */
@SuppressWarnings("all")
public final class IsDeadendQuerySpecification extends BaseGeneratedEMFQuerySpecification<IsDeadendMatcher> {
  private IsDeadendQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IsDeadendQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected IsDeadendMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return IsDeadendMatcher.on(engine);
  }
  
  @Override
  public IsDeadendMatch newEmptyMatch() {
    return IsDeadendMatch.newEmptyMatch();
  }
  
  @Override
  public IsDeadendMatch newMatch(final Object... parameters) {
    return IsDeadendMatch.newMatch((railway.TrackElement) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static IsDeadendQuerySpecification INSTANCE = make();
    
    public static IsDeadendQuerySpecification make() {
      return new IsDeadendQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static IsDeadendQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.isDeadend";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("e");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("e", "railway.TrackElement"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_e = body.getOrCreateVariableByName("e");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_e, "e")
      	));
      	new TypeUnary(body, var_e, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
