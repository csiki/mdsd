package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.util.TrainIsNotOnNodeQuerySpecification;
import java.util.Arrays;
import java.util.List;
import org.eclipse.incquery.runtime.api.IPatternMatch;
import org.eclipse.incquery.runtime.api.impl.BasePatternMatch;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import railway.Node;

/**
 * Pattern-specific match representation of the hu.bme.mit.mdsd.railway.trainIsNotOnNode pattern,
 * to be used in conjunction with {@link TrainIsNotOnNodeMatcher}.
 * 
 * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
 * Each instance is a (possibly partial) substitution of pattern parameters,
 * usable to represent a match of the pattern in the result of a query,
 * or to specify the bound (fixed) input parameters when issuing a query.
 * 
 * @see TrainIsNotOnNodeMatcher
 * @see TrainIsNotOnNodeProcessor
 * 
 */
@SuppressWarnings("all")
public abstract class TrainIsNotOnNodeMatch extends BasePatternMatch {
  private Node fNode;
  
  private static List<String> parameterNames = makeImmutableList("node");
  
  private TrainIsNotOnNodeMatch(final Node pNode) {
    this.fNode = pNode;
  }
  
  @Override
  public Object get(final String parameterName) {
    if ("node".equals(parameterName)) return this.fNode;
    return null;
  }
  
  public Node getNode() {
    return this.fNode;
  }
  
  @Override
  public boolean set(final String parameterName, final Object newValue) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    if ("node".equals(parameterName) ) {
    	this.fNode = (railway.Node) newValue;
    	return true;
    }
    return false;
  }
  
  public void setNode(final Node pNode) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    this.fNode = pNode;
  }
  
  @Override
  public String patternName() {
    return "hu.bme.mit.mdsd.railway.trainIsNotOnNode";
  }
  
  @Override
  public List<String> parameterNames() {
    return TrainIsNotOnNodeMatch.parameterNames;
  }
  
  @Override
  public Object[] toArray() {
    return new Object[]{fNode};
  }
  
  @Override
  public TrainIsNotOnNodeMatch toImmutable() {
    return isMutable() ? newMatch(fNode) : this;
  }
  
  @Override
  public String prettyPrint() {
    StringBuilder result = new StringBuilder();
    result.append("\"node\"=" + prettyPrintValue(fNode)
    );
    return result.toString();
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fNode == null) ? 0 : fNode.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
    	return true;
    if (!(obj instanceof TrainIsNotOnNodeMatch)) { // this should be infrequent
    	if (obj == null) {
    		return false;
    	}
    	if (!(obj instanceof IPatternMatch)) {
    		return false;
    	}
    	IPatternMatch otherSig  = (IPatternMatch) obj;
    	if (!specification().equals(otherSig.specification()))
    		return false;
    	return Arrays.deepEquals(toArray(), otherSig.toArray());
    }
    TrainIsNotOnNodeMatch other = (TrainIsNotOnNodeMatch) obj;
    if (fNode == null) {if (other.fNode != null) return false;}
    else if (!fNode.equals(other.fNode)) return false;
    return true;
  }
  
  @Override
  public TrainIsNotOnNodeQuerySpecification specification() {
    try {
    	return TrainIsNotOnNodeQuerySpecification.instance();
    } catch (IncQueryException ex) {
     	// This cannot happen, as the match object can only be instantiated if the query specification exists
     	throw new IllegalStateException (ex);
    }
  }
  
  /**
   * Returns an empty, mutable match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @return the empty match.
   * 
   */
  public static TrainIsNotOnNodeMatch newEmptyMatch() {
    return new Mutable(null);
  }
  
  /**
   * Returns a mutable (partial) match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @return the new, mutable (partial) match object.
   * 
   */
  public static TrainIsNotOnNodeMatch newMutableMatch(final Node pNode) {
    return new Mutable(pNode);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public static TrainIsNotOnNodeMatch newMatch(final Node pNode) {
    return new Immutable(pNode);
  }
  
  private static final class Mutable extends TrainIsNotOnNodeMatch {
    Mutable(final Node pNode) {
      super(pNode);
    }
    
    @Override
    public boolean isMutable() {
      return true;
    }
  }
  
  private static final class Immutable extends TrainIsNotOnNodeMatch {
    Immutable(final Node pNode) {
      super(pNode);
    }
    
    @Override
    public boolean isMutable() {
      return false;
    }
  }
}
