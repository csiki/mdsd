package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatch;
import hu.bme.mit.mdsd.railway.DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher;
import hu.bme.mit.mdsd.railway.util.IsSemaphoreQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsSwitchQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher in a type-safe way.
 * 
 * @see DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher
 * @see DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatch
 * 
 */
@SuppressWarnings("all")
public final class DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification extends BaseGeneratedEMFQuerySpecification<DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher> {
  private DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher.on(engine);
  }
  
  @Override
  public DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatch newEmptyMatch() {
    return DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatch.newEmptyMatch();
  }
  
  @Override
  public DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatch newMatch(final Object... parameters) {
    return DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatch.newMatch((railway.TrackElement) parameters[0], (railway.TrackElement) parameters[1]);
  }
  
  private static class LazyHolder {
    private final static DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification INSTANCE = make();
    
    public static DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification make() {
      return new DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("e1","e2");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("e1", "railway.TrackElement"),new PParameter("e2", "railway.TrackElement"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_e1, "e1"),
      				
      		new ExportedParameter(body, var_e2, "e2")
      	));
      	new NegativePatternCall(body, new FlatTuple(var_e1), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	new NegativePatternCall(body, new FlatTuple(var_e2), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	new NegativePatternCall(body, new FlatTuple(var_e1), IsSwitchQuerySpecification.instance().getInternalQueryRepresentation());
      	new NegativePatternCall(body, new FlatTuple(var_e2), IsSwitchQuerySpecification.instance().getInternalQueryRepresentation());
      	new TypeBinary(body, CONTEXT, var_e1, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "connectsTo"), "hu.bme.mit.mdsd.railway/TrackElement.connectsTo");
      	bodies.add(body);
      }
      {
      	PBody body = new PBody(this);
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_e1, "e1"),
      				
      		new ExportedParameter(body, var_e2, "e2")
      	));
      	new NegativePatternCall(body, new FlatTuple(var_e1), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	new NegativePatternCall(body, new FlatTuple(var_e2), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	new PositivePatternCall(body, new FlatTuple(var_e1), IsSwitchQuerySpecification.instance().getInternalQueryRepresentation());
      	new NegativePatternCall(body, new FlatTuple(var_e2), IsSwitchQuerySpecification.instance().getInternalQueryRepresentation());
      	new TypeBinary(body, CONTEXT, var_e1, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "connectsTo"), "hu.bme.mit.mdsd.railway/TrackElement.connectsTo");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
