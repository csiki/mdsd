package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.DirectlyConnectedBotNosSemaphoreMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.TrackElement;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.directlyConnectedBotNosSemaphore pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class DirectlyConnectedBotNosSemaphoreProcessor implements IMatchProcessor<DirectlyConnectedBotNosSemaphoreMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pE1 the value of pattern parameter e1 in the currently processed match
   * @param pE2 the value of pattern parameter e2 in the currently processed match
   * 
   */
  public abstract void process(final TrackElement pE1, final TrackElement pE2);
  
  @Override
  public void process(final DirectlyConnectedBotNosSemaphoreMatch match) {
    process(match.getE1(), match.getE2());
  }
}
