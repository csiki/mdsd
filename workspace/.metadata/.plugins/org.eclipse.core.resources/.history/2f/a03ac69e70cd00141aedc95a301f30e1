package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.TrainFromToDeadendMatch;
import hu.bme.mit.mdsd.railway.TrainFromToDeadendMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate TrainFromToDeadendMatcher in a type-safe way.
 * 
 * @see TrainFromToDeadendMatcher
 * @see TrainFromToDeadendMatch
 * 
 */
@SuppressWarnings("all")
public final class TrainFromToDeadendQuerySpecification extends BaseGeneratedEMFQuerySpecification<TrainFromToDeadendMatcher> {
  private TrainFromToDeadendQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static TrainFromToDeadendQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected TrainFromToDeadendMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return TrainFromToDeadendMatcher.on(engine);
  }
  
  @Override
  public TrainFromToDeadendMatch newEmptyMatch() {
    return TrainFromToDeadendMatch.newEmptyMatch();
  }
  
  @Override
  public TrainFromToDeadendMatch newMatch(final Object... parameters) {
    return TrainFromToDeadendMatch.newMatch((railway.Train) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static TrainFromToDeadendQuerySpecification INSTANCE = make();
    
    public static TrainFromToDeadendQuerySpecification make() {
      return new TrainFromToDeadendQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static TrainFromToDeadendQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.trainFromToDeadend";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("train");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("train", "railway.Train"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_train = body.getOrCreateVariableByName("train");
      	PVariable var_trackElements = body.getOrCreateVariableByName("trackElements");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_train, "train")
      	));
      	new TypeBinary(body, CONTEXT, var_train, var_trackElements, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
