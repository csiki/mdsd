package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.IsGoodConnectedMatch;
import hu.bme.mit.mdsd.railway.IsGoodConnectedMatcher;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedButNotSemaphoreQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.BinaryTransitiveClosure;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate IsGoodConnectedMatcher in a type-safe way.
 * 
 * @see IsGoodConnectedMatcher
 * @see IsGoodConnectedMatch
 * 
 */
@SuppressWarnings("all")
public final class IsGoodConnectedQuerySpecification extends BaseGeneratedEMFQuerySpecification<IsGoodConnectedMatcher> {
  private IsGoodConnectedQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IsGoodConnectedQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected IsGoodConnectedMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return IsGoodConnectedMatcher.on(engine);
  }
  
  @Override
  public IsGoodConnectedMatch newEmptyMatch() {
    return IsGoodConnectedMatch.newEmptyMatch();
  }
  
  @Override
  public IsGoodConnectedMatch newMatch(final Object... parameters) {
    return IsGoodConnectedMatch.newMatch((railway.Semaphore) parameters[0], (railway.Semaphore) parameters[1]);
  }
  
  private static class LazyHolder {
    private final static IsGoodConnectedQuerySpecification INSTANCE = make();
    
    public static IsGoodConnectedQuerySpecification make() {
      return new IsGoodConnectedQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static IsGoodConnectedQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.isGoodConnected";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("s1","s2");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("s1", "railway.Semaphore"),new PParameter("s2", "railway.Semaphore"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_s1 = body.getOrCreateVariableByName("s1");
      	PVariable var_s2 = body.getOrCreateVariableByName("s2");
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_f1 = body.getOrCreateVariableByName("f1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	PVariable var_f2 = body.getOrCreateVariableByName("f2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_s1, "s1"),
      				
      		new ExportedParameter(body, var_s2, "s2")
      	));
      	new Inequality(body, var_s1, var_s2);
      	new TypeBinary(body, CONTEXT, var_s1, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "elements"), "hu.bme.mit.mdsd.railway/Semaphore.elements");
      	new TypeBinary(body, CONTEXT, var_s1, var_f1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "facing"), "hu.bme.mit.mdsd.railway/Semaphore.facing");
      	new Inequality(body, var_e1, var_f1);
      	new TypeBinary(body, CONTEXT, var_s2, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "elements"), "hu.bme.mit.mdsd.railway/Semaphore.elements");
      	new TypeBinary(body, CONTEXT, var_s2, var_f2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "facing"), "hu.bme.mit.mdsd.railway/Semaphore.facing");
      	new Inequality(body, var_e2, var_f2);
      	new BinaryTransitiveClosure(body, new FlatTuple(var_f1, var_e2), DirectlyConnectedButNotSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      {
      	PBody body = new PBody(this);
      	PVariable var_s1 = body.getOrCreateVariableByName("s1");
      	PVariable var_s2 = body.getOrCreateVariableByName("s2");
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_f1 = body.getOrCreateVariableByName("f1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	PVariable var_f2 = body.getOrCreateVariableByName("f2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_s1, "s1"),
      				
      		new ExportedParameter(body, var_s2, "s2")
      	));
      	new Inequality(body, var_s1, var_s2);
      	new TypeBinary(body, CONTEXT, var_s1, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "elements"), "hu.bme.mit.mdsd.railway/Semaphore.elements");
      	new TypeBinary(body, CONTEXT, var_s1, var_f1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "facing"), "hu.bme.mit.mdsd.railway/Semaphore.facing");
      	new Inequality(body, var_e1, var_f1);
      	new TypeBinary(body, CONTEXT, var_s2, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "elements"), "hu.bme.mit.mdsd.railway/Semaphore.elements");
      	new TypeBinary(body, CONTEXT, var_s2, var_f2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "facing"), "hu.bme.mit.mdsd.railway/Semaphore.facing");
      	new Inequality(body, var_e2, var_f2);
      	new BinaryTransitiveClosure(body, new FlatTuple(var_e2, var_f1), DirectlyConnectedButNotSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      {
      	PBody body = new PBody(this);
      	PVariable var_s1 = body.getOrCreateVariableByName("s1");
      	PVariable var_s2 = body.getOrCreateVariableByName("s2");
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_s1, "s1"),
      				
      		new ExportedParameter(body, var_s2, "s2")
      	));
      	new Inequality(body, var_s1, var_s2);
      	new TypeBinary(body, CONTEXT, var_s1, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "elements"), "hu.bme.mit.mdsd.railway/Semaphore.elements");
      	new TypeBinary(body, CONTEXT, var_s1, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "facing"), "hu.bme.mit.mdsd.railway/Semaphore.facing");
      	new Inequality(body, var_e1, var_e2);
      	new TypeBinary(body, CONTEXT, var_s2, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "elements"), "hu.bme.mit.mdsd.railway/Semaphore.elements");
      	new TypeBinary(body, CONTEXT, var_s2, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Semaphore", "facing"), "hu.bme.mit.mdsd.railway/Semaphore.facing");
      	new Inequality(body, var_e2, var_e1);
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
