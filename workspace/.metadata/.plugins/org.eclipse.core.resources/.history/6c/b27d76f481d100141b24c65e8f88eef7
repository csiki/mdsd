package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.CountSemaphoreMatcher;
import hu.bme.mit.mdsd.railway.CountTrainMatcher;
import hu.bme.mit.mdsd.railway.DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher;
import hu.bme.mit.mdsd.railway.DirectlyConnectedButNotSemaphoreMatcher;
import hu.bme.mit.mdsd.railway.DirectlyConnectedMatcher;
import hu.bme.mit.mdsd.railway.FindTrainsOnNodeMatcher;
import hu.bme.mit.mdsd.railway.GetAllTrainPairsOnSempahorelessPathMatcher;
import hu.bme.mit.mdsd.railway.IsAtLeastThreeDeadEndInTrainPathMatcher;
import hu.bme.mit.mdsd.railway.IsAtLeastTwoDeadEndInTrainPathMatcher;
import hu.bme.mit.mdsd.railway.IsDangerouslyConnectedMatcher;
import hu.bme.mit.mdsd.railway.IsEveryTrainPathValidMatcher;
import hu.bme.mit.mdsd.railway.IsGoodConnectedMatcher;
import hu.bme.mit.mdsd.railway.IsMoreSemaphoreThanTrainMatcher;
import hu.bme.mit.mdsd.railway.IsSemaphoreMatcher;
import hu.bme.mit.mdsd.railway.IsSwitchMatcher;
import hu.bme.mit.mdsd.railway.NotEveryTrainPathValidMatcher;
import hu.bme.mit.mdsd.railway.TrainsHaveExactlyTwoDeadendsInPathMatcher;
import hu.bme.mit.mdsd.railway.util.CountSemaphoreQuerySpecification;
import hu.bme.mit.mdsd.railway.util.CountTrainQuerySpecification;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedButNotSemaphoreQuerySpecification;
import hu.bme.mit.mdsd.railway.util.DirectlyConnectedQuerySpecification;
import hu.bme.mit.mdsd.railway.util.FindTrainsOnNodeQuerySpecification;
import hu.bme.mit.mdsd.railway.util.GetAllTrainPairsOnSempahorelessPathQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsAtLeastThreeDeadEndInTrainPathQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsAtLeastTwoDeadEndInTrainPathQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsDangerouslyConnectedQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsEveryTrainPathValidQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsGoodConnectedQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsMoreSemaphoreThanTrainQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsSemaphoreQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsSwitchQuerySpecification;
import hu.bme.mit.mdsd.railway.util.NotEveryTrainPathValidQuerySpecification;
import hu.bme.mit.mdsd.railway.util.TrainsHaveExactlyTwoDeadendsInPathQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedPatternGroup;
import org.eclipse.incquery.runtime.exception.IncQueryException;

/**
 * A pattern group formed of all patterns defined in queries.eiq.
 * 
 * <p>Use the static instance as any {@link org.eclipse.incquery.runtime.api.IPatternGroup}, to conveniently prepare
 * an EMF-IncQuery engine for matching all patterns originally defined in file queries.eiq,
 * in order to achieve better performance than one-by-one on-demand matcher initialization.
 * 
 * <p> From package hu.bme.mit.mdsd.railway, the group contains the definition of the following patterns: <ul>
 * <li>isSemaphore</li>
 * <li>isSwitch</li>
 * <li>countSemaphore</li>
 * <li>countTrain</li>
 * <li>directlyConnected</li>
 * <li>directlyConnectedButNotSemaphore</li>
 * <li>directlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected</li>
 * <li>findTrainsOnNode</li>
 * <li>getAllTrainPairsOnSempahorelessPath</li>
 * <li>isDangerouslyConnected</li>
 * <li>isGoodConnected</li>
 * <li>isMoreSemaphoreThanTrain</li>
 * <li>trainsHaveExactlyTwoDeadendsInPath</li>
 * <li>isAtLeastThreeDeadEndInTrainPath</li>
 * <li>isAtLeastTwoDeadEndInTrainPath</li>
 * <li>notEveryTrainPathValid</li>
 * <li>isEveryTrainPathValid</li>
 * </ul>
 * 
 * @see IPatternGroup
 * 
 */
@SuppressWarnings("all")
public final class Queries extends BaseGeneratedPatternGroup {
  /**
   * Access the pattern group.
   * 
   * @return the singleton instance of the group
   * @throws IncQueryException if there was an error loading the generated code of pattern specifications
   * 
   */
  public static Queries instance() throws IncQueryException {
    if (INSTANCE == null) {
    	INSTANCE = new Queries();
    }
    return INSTANCE;
  }
  
  private static Queries INSTANCE;
  
  private Queries() throws IncQueryException {
    querySpecifications.add(IsSemaphoreQuerySpecification.instance());
    querySpecifications.add(IsSwitchQuerySpecification.instance());
    querySpecifications.add(CountSemaphoreQuerySpecification.instance());
    querySpecifications.add(CountTrainQuerySpecification.instance());
    querySpecifications.add(DirectlyConnectedQuerySpecification.instance());
    querySpecifications.add(DirectlyConnectedButNotSemaphoreQuerySpecification.instance());
    querySpecifications.add(DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification.instance());
    querySpecifications.add(FindTrainsOnNodeQuerySpecification.instance());
    querySpecifications.add(GetAllTrainPairsOnSempahorelessPathQuerySpecification.instance());
    querySpecifications.add(IsDangerouslyConnectedQuerySpecification.instance());
    querySpecifications.add(IsGoodConnectedQuerySpecification.instance());
    querySpecifications.add(IsMoreSemaphoreThanTrainQuerySpecification.instance());
    querySpecifications.add(TrainsHaveExactlyTwoDeadendsInPathQuerySpecification.instance());
    querySpecifications.add(IsAtLeastThreeDeadEndInTrainPathQuerySpecification.instance());
    querySpecifications.add(IsAtLeastTwoDeadEndInTrainPathQuerySpecification.instance());
    querySpecifications.add(NotEveryTrainPathValidQuerySpecification.instance());
    querySpecifications.add(IsEveryTrainPathValidQuerySpecification.instance());
  }
  
  public IsSemaphoreQuerySpecification getIsSemaphore() throws IncQueryException {
    return IsSemaphoreQuerySpecification.instance();
  }
  
  public IsSemaphoreMatcher getIsSemaphore(final IncQueryEngine engine) throws IncQueryException {
    return IsSemaphoreMatcher.on(engine);
  }
  
  public IsSwitchQuerySpecification getIsSwitch() throws IncQueryException {
    return IsSwitchQuerySpecification.instance();
  }
  
  public IsSwitchMatcher getIsSwitch(final IncQueryEngine engine) throws IncQueryException {
    return IsSwitchMatcher.on(engine);
  }
  
  public CountSemaphoreQuerySpecification getCountSemaphore() throws IncQueryException {
    return CountSemaphoreQuerySpecification.instance();
  }
  
  public CountSemaphoreMatcher getCountSemaphore(final IncQueryEngine engine) throws IncQueryException {
    return CountSemaphoreMatcher.on(engine);
  }
  
  public CountTrainQuerySpecification getCountTrain() throws IncQueryException {
    return CountTrainQuerySpecification.instance();
  }
  
  public CountTrainMatcher getCountTrain(final IncQueryEngine engine) throws IncQueryException {
    return CountTrainMatcher.on(engine);
  }
  
  public DirectlyConnectedQuerySpecification getDirectlyConnected() throws IncQueryException {
    return DirectlyConnectedQuerySpecification.instance();
  }
  
  public DirectlyConnectedMatcher getDirectlyConnected(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedMatcher.on(engine);
  }
  
  public DirectlyConnectedButNotSemaphoreQuerySpecification getDirectlyConnectedButNotSemaphore() throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreQuerySpecification.instance();
  }
  
  public DirectlyConnectedButNotSemaphoreMatcher getDirectlyConnectedButNotSemaphore(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreMatcher.on(engine);
  }
  
  public DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification getDirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected() throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedQuerySpecification.instance();
  }
  
  public DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher getDirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnected(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreAndSwitchIsCurrentlyConnectedMatcher.on(engine);
  }
  
  public FindTrainsOnNodeQuerySpecification getFindTrainsOnNode() throws IncQueryException {
    return FindTrainsOnNodeQuerySpecification.instance();
  }
  
  public FindTrainsOnNodeMatcher getFindTrainsOnNode(final IncQueryEngine engine) throws IncQueryException {
    return FindTrainsOnNodeMatcher.on(engine);
  }
  
  public GetAllTrainPairsOnSempahorelessPathQuerySpecification getGetAllTrainPairsOnSempahorelessPath() throws IncQueryException {
    return GetAllTrainPairsOnSempahorelessPathQuerySpecification.instance();
  }
  
  public GetAllTrainPairsOnSempahorelessPathMatcher getGetAllTrainPairsOnSempahorelessPath(final IncQueryEngine engine) throws IncQueryException {
    return GetAllTrainPairsOnSempahorelessPathMatcher.on(engine);
  }
  
  public IsDangerouslyConnectedQuerySpecification getIsDangerouslyConnected() throws IncQueryException {
    return IsDangerouslyConnectedQuerySpecification.instance();
  }
  
  public IsDangerouslyConnectedMatcher getIsDangerouslyConnected(final IncQueryEngine engine) throws IncQueryException {
    return IsDangerouslyConnectedMatcher.on(engine);
  }
  
  public IsGoodConnectedQuerySpecification getIsGoodConnected() throws IncQueryException {
    return IsGoodConnectedQuerySpecification.instance();
  }
  
  public IsGoodConnectedMatcher getIsGoodConnected(final IncQueryEngine engine) throws IncQueryException {
    return IsGoodConnectedMatcher.on(engine);
  }
  
  public IsMoreSemaphoreThanTrainQuerySpecification getIsMoreSemaphoreThanTrain() throws IncQueryException {
    return IsMoreSemaphoreThanTrainQuerySpecification.instance();
  }
  
  public IsMoreSemaphoreThanTrainMatcher getIsMoreSemaphoreThanTrain(final IncQueryEngine engine) throws IncQueryException {
    return IsMoreSemaphoreThanTrainMatcher.on(engine);
  }
  
  public TrainsHaveExactlyTwoDeadendsInPathQuerySpecification getTrainsHaveExactlyTwoDeadendsInPath() throws IncQueryException {
    return TrainsHaveExactlyTwoDeadendsInPathQuerySpecification.instance();
  }
  
  public TrainsHaveExactlyTwoDeadendsInPathMatcher getTrainsHaveExactlyTwoDeadendsInPath(final IncQueryEngine engine) throws IncQueryException {
    return TrainsHaveExactlyTwoDeadendsInPathMatcher.on(engine);
  }
  
  public IsAtLeastThreeDeadEndInTrainPathQuerySpecification getIsAtLeastThreeDeadEndInTrainPath() throws IncQueryException {
    return IsAtLeastThreeDeadEndInTrainPathQuerySpecification.instance();
  }
  
  public IsAtLeastThreeDeadEndInTrainPathMatcher getIsAtLeastThreeDeadEndInTrainPath(final IncQueryEngine engine) throws IncQueryException {
    return IsAtLeastThreeDeadEndInTrainPathMatcher.on(engine);
  }
  
  public IsAtLeastTwoDeadEndInTrainPathQuerySpecification getIsAtLeastTwoDeadEndInTrainPath() throws IncQueryException {
    return IsAtLeastTwoDeadEndInTrainPathQuerySpecification.instance();
  }
  
  public IsAtLeastTwoDeadEndInTrainPathMatcher getIsAtLeastTwoDeadEndInTrainPath(final IncQueryEngine engine) throws IncQueryException {
    return IsAtLeastTwoDeadEndInTrainPathMatcher.on(engine);
  }
  
  public NotEveryTrainPathValidQuerySpecification getNotEveryTrainPathValid() throws IncQueryException {
    return NotEveryTrainPathValidQuerySpecification.instance();
  }
  
  public NotEveryTrainPathValidMatcher getNotEveryTrainPathValid(final IncQueryEngine engine) throws IncQueryException {
    return NotEveryTrainPathValidMatcher.on(engine);
  }
  
  public IsEveryTrainPathValidQuerySpecification getIsEveryTrainPathValid() throws IncQueryException {
    return IsEveryTrainPathValidQuerySpecification.instance();
  }
  
  public IsEveryTrainPathValidMatcher getIsEveryTrainPathValid(final IncQueryEngine engine) throws IncQueryException {
    return IsEveryTrainPathValidMatcher.on(engine);
  }
}
