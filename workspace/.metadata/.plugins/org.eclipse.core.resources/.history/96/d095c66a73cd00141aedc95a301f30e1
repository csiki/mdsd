package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.TrainsAreNotOnNodesMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Node;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.trainsAreNotOnNodes pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class TrainsAreNotOnNodesProcessor implements IMatchProcessor<TrainsAreNotOnNodesMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pN the value of pattern parameter n in the currently processed match
   * 
   */
  public abstract void process(final Node pN);
  
  @Override
  public void process(final TrainsAreNotOnNodesMatch match) {
    process(match.getN());
  }
}
