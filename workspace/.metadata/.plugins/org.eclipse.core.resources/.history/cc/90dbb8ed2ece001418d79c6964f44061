package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.FindTrainsOnNodeMatch;
import hu.bme.mit.mdsd.railway.FindTrainsOnNodeMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate FindTrainsOnNodeMatcher in a type-safe way.
 * 
 * @see FindTrainsOnNodeMatcher
 * @see FindTrainsOnNodeMatch
 * 
 */
@SuppressWarnings("all")
public final class FindTrainsOnNodeQuerySpecification extends BaseGeneratedEMFQuerySpecification<FindTrainsOnNodeMatcher> {
  private FindTrainsOnNodeQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static FindTrainsOnNodeQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected FindTrainsOnNodeMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return FindTrainsOnNodeMatcher.on(engine);
  }
  
  @Override
  public FindTrainsOnNodeMatch newEmptyMatch() {
    return FindTrainsOnNodeMatch.newEmptyMatch();
  }
  
  @Override
  public FindTrainsOnNodeMatch newMatch(final Object... parameters) {
    return FindTrainsOnNodeMatch.newMatch((railway.Node) parameters[0], (railway.Train) parameters[1]);
  }
  
  private static class LazyHolder {
    private final static FindTrainsOnNodeQuerySpecification INSTANCE = make();
    
    public static FindTrainsOnNodeQuerySpecification make() {
      return new FindTrainsOnNodeQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static FindTrainsOnNodeQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.findTrainsOnNode";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("node","train");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("node", "railway.Node"),new PParameter("train", "railway.Train"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_node = body.getOrCreateVariableByName("node");
      	PVariable var_train = body.getOrCreateVariableByName("train");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_node, "node"),
      				
      		new ExportedParameter(body, var_train, "train")
      	));
      	new TypeUnary(body, var_node, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Node"), "hu.bme.mit.mdsd.railway/Node");
      	new TypeBinary(body, CONTEXT, var_node, var_train, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "train"), "hu.bme.mit.mdsd.railway/TrackElement.train");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
