package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.GetAllConnectedSemaphorePairsMatch;
import hu.bme.mit.mdsd.railway.GetAllConnectedSemaphorePairsMatcher;
import hu.bme.mit.mdsd.railway.util.IsConnectedQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate GetAllConnectedSemaphorePairsMatcher in a type-safe way.
 * 
 * @see GetAllConnectedSemaphorePairsMatcher
 * @see GetAllConnectedSemaphorePairsMatch
 * 
 */
@SuppressWarnings("all")
public final class GetAllConnectedSemaphorePairsQuerySpecification extends BaseGeneratedEMFQuerySpecification<GetAllConnectedSemaphorePairsMatcher> {
  private GetAllConnectedSemaphorePairsQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static GetAllConnectedSemaphorePairsQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected GetAllConnectedSemaphorePairsMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return GetAllConnectedSemaphorePairsMatcher.on(engine);
  }
  
  @Override
  public GetAllConnectedSemaphorePairsMatch newEmptyMatch() {
    return GetAllConnectedSemaphorePairsMatch.newEmptyMatch();
  }
  
  @Override
  public GetAllConnectedSemaphorePairsMatch newMatch(final Object... parameters) {
    return GetAllConnectedSemaphorePairsMatch.newMatch((railway.Semaphore) parameters[0], (railway.Semaphore) parameters[1]);
  }
  
  private static class LazyHolder {
    private final static GetAllConnectedSemaphorePairsQuerySpecification INSTANCE = make();
    
    public static GetAllConnectedSemaphorePairsQuerySpecification make() {
      return new GetAllConnectedSemaphorePairsQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static GetAllConnectedSemaphorePairsQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.getAllConnectedSemaphorePairs";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("s1","s2");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("s1", "railway.Semaphore"),new PParameter("s2", "railway.Semaphore"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_s1 = body.getOrCreateVariableByName("s1");
      	PVariable var_s2 = body.getOrCreateVariableByName("s2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_s1, "s1"),
      				
      		new ExportedParameter(body, var_s2, "s2")
      	));
      	new TypeUnary(body, var_s2, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Semaphore"), "hu.bme.mit.mdsd.railway/Semaphore");
      	new TypeUnary(body, var_s1, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Semaphore"), "hu.bme.mit.mdsd.railway/Semaphore");
      	new PositivePatternCall(body, new FlatTuple(var_s1, var_s2), IsConnectedQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
