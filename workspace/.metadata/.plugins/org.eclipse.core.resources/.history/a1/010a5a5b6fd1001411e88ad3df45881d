/**
 */
package railway;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semaphore</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link railway.Semaphore#getElements <em>Elements</em>}</li>
 *   <li>{@link railway.Semaphore#getState <em>State</em>}</li>
 *   <li>{@link railway.Semaphore#getFacing <em>Facing</em>}</li>
 *   <li>{@link railway.Semaphore#getNextState <em>Next State</em>}</li>
 * </ul>
 * </p>
 *
 * @see railway.RailwayPackage#getSemaphore()
 * @model
 * @generated
 */
public interface Semaphore extends Node {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' reference list.
	 * The list contents are of type {@link railway.TrackElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' reference list.
	 * @see railway.RailwayPackage#getSemaphore_Elements()
	 * @model lower="2" upper="2"
	 * @generated
	 */
	EList<TrackElement> getElements();

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * The default value is <code>"GO"</code>.
	 * The literals are from the enumeration {@link railway.Signal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see railway.Signal
	 * @see #setState(Signal)
	 * @see railway.RailwayPackage#getSemaphore_State()
	 * @model default="GO"
	 * @generated
	 */
	Signal getState();

	/**
	 * Sets the value of the '{@link railway.Semaphore#getState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see railway.Signal
	 * @see #getState()
	 * @generated
	 */
	void setState(Signal value);

	/**
	 * Returns the value of the '<em><b>Facing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Facing</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Facing</em>' reference.
	 * @see #setFacing(TrackElement)
	 * @see railway.RailwayPackage#getSemaphore_Facing()
	 * @model required="true"
	 * @generated
	 */
	TrackElement getFacing();

	/**
	 * Sets the value of the '{@link railway.Semaphore#getFacing <em>Facing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Facing</em>' reference.
	 * @see #getFacing()
	 * @generated
	 */
	void setFacing(TrackElement value);

	/**
	 * Returns the value of the '<em><b>Next State</b></em>' attribute.
	 * The literals are from the enumeration {@link railway.Signal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next State</em>' attribute.
	 * @see railway.Signal
	 * @see #setNextState(Signal)
	 * @see railway.RailwayPackage#getSemaphore_NextState()
	 * @model
	 * @generated
	 */
	Signal getNextState();

	/**
	 * Sets the value of the '{@link railway.Semaphore#getNextState <em>Next State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next State</em>' attribute.
	 * @see railway.Signal
	 * @see #getNextState()
	 * @generated
	 */
	void setNextState(Signal value);

} // Semaphore
