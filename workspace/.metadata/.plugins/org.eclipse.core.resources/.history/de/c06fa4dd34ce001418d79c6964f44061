package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.IsTrainMatch;
import hu.bme.mit.mdsd.railway.IsTrainMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate IsTrainMatcher in a type-safe way.
 * 
 * @see IsTrainMatcher
 * @see IsTrainMatch
 * 
 */
@SuppressWarnings("all")
public final class IsTrainQuerySpecification extends BaseGeneratedEMFQuerySpecification<IsTrainMatcher> {
  private IsTrainQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IsTrainQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected IsTrainMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return IsTrainMatcher.on(engine);
  }
  
  @Override
  public IsTrainMatch newEmptyMatch() {
    return IsTrainMatch.newEmptyMatch();
  }
  
  @Override
  public IsTrainMatch newMatch(final Object... parameters) {
    return IsTrainMatch.newMatch((railway.Train) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static IsTrainQuerySpecification INSTANCE = make();
    
    public static IsTrainQuerySpecification make() {
      return new IsTrainQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static IsTrainQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.isTrain";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("e");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("e", "railway.Train"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_e = body.getOrCreateVariableByName("e");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_e, "e")
      	));
      	new TypeUnary(body, var_e, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Train"), "hu.bme.mit.mdsd.railway/Train");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
