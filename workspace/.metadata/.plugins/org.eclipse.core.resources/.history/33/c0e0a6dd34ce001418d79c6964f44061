package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.IsTrainMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Train;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.isTrain pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class IsTrainProcessor implements IMatchProcessor<IsTrainMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pE the value of pattern parameter e in the currently processed match
   * 
   */
  public abstract void process(final Train pE);
  
  @Override
  public void process(final IsTrainMatch match) {
    process(match.getE());
  }
}
