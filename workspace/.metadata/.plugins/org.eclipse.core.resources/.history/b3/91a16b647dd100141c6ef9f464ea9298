package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.FindTrainsOnNodeMatch;
import hu.bme.mit.mdsd.railway.util.FindTrainsOnNodeQuerySpecification;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import org.eclipse.incquery.runtime.api.IQuerySpecification;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseMatcher;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.tuple.Tuple;
import org.eclipse.incquery.runtime.util.IncQueryLoggingUtil;
import railway.Node;
import railway.Train;

/**
 * Generated pattern matcher API of the hu.bme.mit.mdsd.railway.findTrainsOnNode pattern,
 * providing pattern-specific query methods.
 * 
 * <p>Use the pattern matcher on a given model via {@link #on(IncQueryEngine)},
 * e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}.
 * 
 * <p>Matches of the pattern will be represented as {@link FindTrainsOnNodeMatch}.
 * 
 * <p>Original source:
 * <code><pre>
 * // metamodellben nem kifejezhet� de �rtelmetlen esetek kisz�r�se:
 * // semmibe vezet� s�nszakasz -- tudja a modell?
 * 
 * // vonat csak sectionon lehet
 * 
 * {@literal @}Constraint(severity="error",
 * 	message="Train cannot be on a node!",
 * 	location=node,
 * 	targetEditorId = "railway.presentation.RailwayEditorID"
 * )
 * pattern findTrainsOnNode(node : Node, train : Train) {
 * 	Node.train(node, train);
 * }
 * </pre></code>
 * 
 * @see FindTrainsOnNodeMatch
 * @see FindTrainsOnNodeProcessor
 * @see FindTrainsOnNodeQuerySpecification
 * 
 */
@SuppressWarnings("all")
public class FindTrainsOnNodeMatcher extends BaseMatcher<FindTrainsOnNodeMatch> {
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * 
   */
  public static FindTrainsOnNodeMatcher on(final IncQueryEngine engine) throws IncQueryException {
    // check if matcher already exists
    FindTrainsOnNodeMatcher matcher = engine.getExistingMatcher(querySpecification());
    if (matcher == null) {
    	matcher = new FindTrainsOnNodeMatcher(engine);
    	// do not have to "put" it into engine.matchers, reportMatcherInitialized() will take care of it
    }
    return matcher;
  }
  
  private final static int POSITION_NODE = 0;
  
  private final static int POSITION_TRAIN = 1;
  
  private final static Logger LOGGER = IncQueryLoggingUtil.getLogger(FindTrainsOnNodeMatcher.class);
  
  /**
   * Initializes the pattern matcher over a given EMF model root (recommended: Resource or ResourceSet).
   * If a pattern matcher is already constructed with the same root, only a light-weight reference is returned.
   * The scope of pattern matching will be the given EMF model root and below (see FAQ for more precise definition).
   * The match set will be incrementally refreshed upon updates from this scope.
   * <p>The matcher will be created within the managed {@link IncQueryEngine} belonging to the EMF model root, so
   * multiple matchers will reuse the same engine and benefit from increased performance and reduced memory footprint.
   * @param emfRoot the root of the EMF containment hierarchy where the pattern matcher will operate. Recommended: Resource or ResourceSet.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead, e.g. in conjunction with {@link IncQueryEngine#on(Notifier)}
   * 
   */
  @Deprecated
  public FindTrainsOnNodeMatcher(final Notifier emfRoot) throws IncQueryException {
    this(IncQueryEngine.on(emfRoot));
  }
  
  /**
   * Initializes the pattern matcher within an existing EMF-IncQuery engine.
   * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
   * The match set will be incrementally refreshed upon updates.
   * @param engine the existing EMF-IncQuery engine in which this matcher will be created.
   * @throws IncQueryException if an error occurs during pattern matcher creation
   * @deprecated use {@link #on(IncQueryEngine)} instead
   * 
   */
  @Deprecated
  public FindTrainsOnNodeMatcher(final IncQueryEngine engine) throws IncQueryException {
    super(engine, querySpecification());
  }
  
  /**
   * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param pTrain the fixed value of pattern parameter train, or null if not bound.
   * @return matches represented as a FindTrainsOnNodeMatch object.
   * 
   */
  public Collection<FindTrainsOnNodeMatch> getAllMatches(final Node pNode, final Train pTrain) {
    return rawGetAllMatches(new Object[]{pNode, pTrain});
  }
  
  /**
   * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param pTrain the fixed value of pattern parameter train, or null if not bound.
   * @return a match represented as a FindTrainsOnNodeMatch object, or null if no match is found.
   * 
   */
  public FindTrainsOnNodeMatch getOneArbitraryMatch(final Node pNode, final Train pTrain) {
    return rawGetOneArbitraryMatch(new Object[]{pNode, pTrain});
  }
  
  /**
   * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
   * under any possible substitution of the unspecified parameters (if any).
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param pTrain the fixed value of pattern parameter train, or null if not bound.
   * @return true if the input is a valid (partial) match of the pattern.
   * 
   */
  public boolean hasMatch(final Node pNode, final Train pTrain) {
    return rawHasMatch(new Object[]{pNode, pTrain});
  }
  
  /**
   * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param pTrain the fixed value of pattern parameter train, or null if not bound.
   * @return the number of pattern matches found.
   * 
   */
  public int countMatches(final Node pNode, final Train pTrain) {
    return rawCountMatches(new Object[]{pNode, pTrain});
  }
  
  /**
   * Executes the given processor on each match of the pattern that conforms to the given fixed values of some parameters.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param pTrain the fixed value of pattern parameter train, or null if not bound.
   * @param processor the action that will process each pattern match.
   * 
   */
  public void forEachMatch(final Node pNode, final Train pTrain, final IMatchProcessor<? super FindTrainsOnNodeMatch> processor) {
    rawForEachMatch(new Object[]{pNode, pTrain}, processor);
  }
  
  /**
   * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
   * Neither determinism nor randomness of selection is guaranteed.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param pTrain the fixed value of pattern parameter train, or null if not bound.
   * @param processor the action that will process the selected match.
   * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
   * 
   */
  public boolean forOneArbitraryMatch(final Node pNode, final Train pTrain, final IMatchProcessor<? super FindTrainsOnNodeMatch> processor) {
    return rawForOneArbitraryMatch(new Object[]{pNode, pTrain}, processor);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pNode the fixed value of pattern parameter node, or null if not bound.
   * @param pTrain the fixed value of pattern parameter train, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public FindTrainsOnNodeMatch newMatch(final Node pNode, final Train pTrain) {
    return FindTrainsOnNodeMatch.newMatch(pNode, pTrain);
  }
  
  /**
   * Retrieve the set of values that occur in matches for node.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  protected Set<Node> rawAccumulateAllValuesOfnode(final Object[] parameters) {
    Set<Node> results = new HashSet<Node>();
    rawAccumulateAllValues(POSITION_NODE, parameters, results);
    return results;
  }
  
  /**
   * Retrieve the set of values that occur in matches for node.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Node> getAllValuesOfnode() {
    return rawAccumulateAllValuesOfnode(emptyArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for node.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Node> getAllValuesOfnode(final FindTrainsOnNodeMatch partialMatch) {
    return rawAccumulateAllValuesOfnode(partialMatch.toArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for node.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Node> getAllValuesOfnode(final Train pTrain) {
    return rawAccumulateAllValuesOfnode(new Object[]{
    null, 
    pTrain
    });
  }
  
  /**
   * Retrieve the set of values that occur in matches for train.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  protected Set<Train> rawAccumulateAllValuesOftrain(final Object[] parameters) {
    Set<Train> results = new HashSet<Train>();
    rawAccumulateAllValues(POSITION_TRAIN, parameters, results);
    return results;
  }
  
  /**
   * Retrieve the set of values that occur in matches for train.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Train> getAllValuesOftrain() {
    return rawAccumulateAllValuesOftrain(emptyArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for train.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Train> getAllValuesOftrain(final FindTrainsOnNodeMatch partialMatch) {
    return rawAccumulateAllValuesOftrain(partialMatch.toArray());
  }
  
  /**
   * Retrieve the set of values that occur in matches for train.
   * @return the Set of all values, null if no parameter with the given name exists, empty set if there are no matches
   * 
   */
  public Set<Train> getAllValuesOftrain(final Node pNode) {
    return rawAccumulateAllValuesOftrain(new Object[]{
    pNode, 
    null
    });
  }
  
  @Override
  protected FindTrainsOnNodeMatch tupleToMatch(final Tuple t) {
    try {
    	return FindTrainsOnNodeMatch.newMatch((railway.Node) t.get(POSITION_NODE), (railway.Train) t.get(POSITION_TRAIN));
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in tuple not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected FindTrainsOnNodeMatch arrayToMatch(final Object[] match) {
    try {
    	return FindTrainsOnNodeMatch.newMatch((railway.Node) match[POSITION_NODE], (railway.Train) match[POSITION_TRAIN]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  @Override
  protected FindTrainsOnNodeMatch arrayToMatchMutable(final Object[] match) {
    try {
    	return FindTrainsOnNodeMatch.newMutableMatch((railway.Node) match[POSITION_NODE], (railway.Train) match[POSITION_TRAIN]);
    } catch(ClassCastException e) {
    	LOGGER.error("Element(s) in array not properly typed!",e);
    	return null;
    }
  }
  
  /**
   * @return the singleton instance of the query specification of this pattern
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IQuerySpecification<FindTrainsOnNodeMatcher> querySpecification() throws IncQueryException {
    return FindTrainsOnNodeQuerySpecification.instance();
  }
}
