package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.DirectlyConnectedButNotSemaphoreMatch;
import hu.bme.mit.mdsd.railway.DirectlyConnectedButNotSemaphoreMatcher;
import hu.bme.mit.mdsd.railway.util.IsSemaphoreQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate DirectlyConnectedButNotSemaphoreMatcher in a type-safe way.
 * 
 * @see DirectlyConnectedButNotSemaphoreMatcher
 * @see DirectlyConnectedButNotSemaphoreMatch
 * 
 */
@SuppressWarnings("all")
public final class DirectlyConnectedButNotSemaphoreQuerySpecification extends BaseGeneratedEMFQuerySpecification<DirectlyConnectedButNotSemaphoreMatcher> {
  private DirectlyConnectedButNotSemaphoreQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static DirectlyConnectedButNotSemaphoreQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected DirectlyConnectedButNotSemaphoreMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return DirectlyConnectedButNotSemaphoreMatcher.on(engine);
  }
  
  @Override
  public DirectlyConnectedButNotSemaphoreMatch newEmptyMatch() {
    return DirectlyConnectedButNotSemaphoreMatch.newEmptyMatch();
  }
  
  @Override
  public DirectlyConnectedButNotSemaphoreMatch newMatch(final Object... parameters) {
    return DirectlyConnectedButNotSemaphoreMatch.newMatch((railway.TrackElement) parameters[0], (railway.TrackElement) parameters[1]);
  }
  
  private static class LazyHolder {
    private final static DirectlyConnectedButNotSemaphoreQuerySpecification INSTANCE = make();
    
    public static DirectlyConnectedButNotSemaphoreQuerySpecification make() {
      return new DirectlyConnectedButNotSemaphoreQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static DirectlyConnectedButNotSemaphoreQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.directlyConnectedButNotSemaphore";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("e1","e2");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("e1", "railway.TrackElement"),new PParameter("e2", "railway.TrackElement"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_e1, "e1"),
      				
      		new ExportedParameter(body, var_e2, "e2")
      	));
      	new TypeBinary(body, CONTEXT, var_e1, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "connectsTo"), "hu.bme.mit.mdsd.railway/TrackElement.connectsTo");
      	new NegativePatternCall(body, new FlatTuple(var_e1), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	new NegativePatternCall(body, new FlatTuple(var_e2), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      {
      	PBody body = new PBody(this);
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_e1, "e1"),
      				
      		new ExportedParameter(body, var_e2, "e2")
      	));
      	new TypeBinary(body, CONTEXT, var_e2, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "TrackElement", "connectsTo"), "hu.bme.mit.mdsd.railway/TrackElement.connectsTo");
      	new NegativePatternCall(body, new FlatTuple(var_e1), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	new NegativePatternCall(body, new FlatTuple(var_e2), IsSemaphoreQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
