package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.CountSemaphoreMatch;
import hu.bme.mit.mdsd.railway.CountSemaphoreMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate CountSemaphoreMatcher in a type-safe way.
 * 
 * @see CountSemaphoreMatcher
 * @see CountSemaphoreMatch
 * 
 */
@SuppressWarnings("all")
public final class CountSemaphoreQuerySpecification extends BaseGeneratedEMFQuerySpecification<CountSemaphoreMatcher> {
  private CountSemaphoreQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static CountSemaphoreQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected CountSemaphoreMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return CountSemaphoreMatcher.on(engine);
  }
  
  @Override
  public CountSemaphoreMatch newEmptyMatch() {
    return CountSemaphoreMatch.newEmptyMatch();
  }
  
  @Override
  public CountSemaphoreMatch newMatch(final Object... parameters) {
    return CountSemaphoreMatch.newMatch();
  }
  
  private static class LazyHolder {
    private final static CountSemaphoreQuerySpecification INSTANCE = make();
    
    public static CountSemaphoreQuerySpecification make() {
      return new CountSemaphoreQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static CountSemaphoreQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.countSemaphore";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList();
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList();
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var__e = body.getOrCreateVariableByName("_e");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      	));
      	new TypeUnary(body, var__e, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Semaphore"), "hu.bme.mit.mdsd.railway/Semaphore");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
