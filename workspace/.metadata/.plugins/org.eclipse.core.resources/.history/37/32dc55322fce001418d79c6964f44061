package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.IsDeadendMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.TrackElement;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.isDeadend pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class IsDeadendProcessor implements IMatchProcessor<IsDeadendMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pE the value of pattern parameter e in the currently processed match
   * 
   */
  public abstract void process(final TrackElement pE);
  
  @Override
  public void process(final IsDeadendMatch match) {
    process(match.getE());
  }
}
