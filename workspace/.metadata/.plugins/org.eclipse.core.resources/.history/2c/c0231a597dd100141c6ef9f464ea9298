package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.TrainsHaveExactlyTwoDeadendsInPathMatch;
import hu.bme.mit.mdsd.railway.TrainsHaveExactlyTwoDeadendsInPathMatcher;
import hu.bme.mit.mdsd.railway.util.IsAtLeastThreeDeadEndInTrainPathQuerySpecification;
import hu.bme.mit.mdsd.railway.util.IsAtLeastTwoDeadEndInTrainPathQuerySpecification;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.annotations.PAnnotation;
import org.eclipse.incquery.runtime.matchers.psystem.annotations.ParameterReference;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;
import org.eclipse.incquery.runtime.matchers.tuple.FlatTuple;

/**
 * A pattern-specific query specification that can instantiate TrainsHaveExactlyTwoDeadendsInPathMatcher in a type-safe way.
 * 
 * @see TrainsHaveExactlyTwoDeadendsInPathMatcher
 * @see TrainsHaveExactlyTwoDeadendsInPathMatch
 * 
 */
@SuppressWarnings("all")
public final class TrainsHaveExactlyTwoDeadendsInPathQuerySpecification extends BaseGeneratedEMFQuerySpecification<TrainsHaveExactlyTwoDeadendsInPathMatcher> {
  private TrainsHaveExactlyTwoDeadendsInPathQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static TrainsHaveExactlyTwoDeadendsInPathQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected TrainsHaveExactlyTwoDeadendsInPathMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return TrainsHaveExactlyTwoDeadendsInPathMatcher.on(engine);
  }
  
  @Override
  public TrainsHaveExactlyTwoDeadendsInPathMatch newEmptyMatch() {
    return TrainsHaveExactlyTwoDeadendsInPathMatch.newEmptyMatch();
  }
  
  @Override
  public TrainsHaveExactlyTwoDeadendsInPathMatch newMatch(final Object... parameters) {
    return TrainsHaveExactlyTwoDeadendsInPathMatch.newMatch((railway.Train) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static TrainsHaveExactlyTwoDeadendsInPathQuerySpecification INSTANCE = make();
    
    public static TrainsHaveExactlyTwoDeadendsInPathQuerySpecification make() {
      return new TrainsHaveExactlyTwoDeadendsInPathQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static TrainsHaveExactlyTwoDeadendsInPathQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.trainsHaveExactlyTwoDeadendsInPath";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("t");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("t", "railway.Train"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_t = body.getOrCreateVariableByName("t");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_t, "t")
      	));
      	new TypeUnary(body, var_t, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Train"), "hu.bme.mit.mdsd.railway/Train");
      	new NegativePatternCall(body, new FlatTuple(var_t), IsAtLeastTwoDeadEndInTrainPathQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      {
      	PBody body = new PBody(this);
      	PVariable var_t = body.getOrCreateVariableByName("t");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_t, "t")
      	));
      	new TypeUnary(body, var_t, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Train"), "hu.bme.mit.mdsd.railway/Train");
      	new PositivePatternCall(body, new FlatTuple(var_t), IsAtLeastThreeDeadEndInTrainPathQuerySpecification.instance().getInternalQueryRepresentation());
      	bodies.add(body);
      }
      	{
      	PAnnotation annotation = new PAnnotation("Constraint");
      	annotation.addAttribute("severity", "error");
      	annotation.addAttribute("location", new ParameterReference("t"));
      	annotation.addAttribute("message", "Route of the train should contain at least two dead-ends!");
      	addAnnotation(annotation);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
