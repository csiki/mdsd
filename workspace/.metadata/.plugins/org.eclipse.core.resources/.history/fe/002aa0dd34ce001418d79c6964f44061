package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.IsTwoDeadEndInTrainPathMatch;
import hu.bme.mit.mdsd.railway.IsTwoDeadEndInTrainPathMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate IsTwoDeadEndInTrainPathMatcher in a type-safe way.
 * 
 * @see IsTwoDeadEndInTrainPathMatcher
 * @see IsTwoDeadEndInTrainPathMatch
 * 
 */
@SuppressWarnings("all")
public final class IsTwoDeadEndInTrainPathQuerySpecification extends BaseGeneratedEMFQuerySpecification<IsTwoDeadEndInTrainPathMatcher> {
  private IsTwoDeadEndInTrainPathQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IsTwoDeadEndInTrainPathQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected IsTwoDeadEndInTrainPathMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return IsTwoDeadEndInTrainPathMatcher.on(engine);
  }
  
  @Override
  public IsTwoDeadEndInTrainPathMatch newEmptyMatch() {
    return IsTwoDeadEndInTrainPathMatch.newEmptyMatch();
  }
  
  @Override
  public IsTwoDeadEndInTrainPathMatch newMatch(final Object... parameters) {
    return IsTwoDeadEndInTrainPathMatch.newMatch((railway.Train) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static IsTwoDeadEndInTrainPathQuerySpecification INSTANCE = make();
    
    public static IsTwoDeadEndInTrainPathQuerySpecification make() {
      return new IsTwoDeadEndInTrainPathQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static IsTwoDeadEndInTrainPathQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.isTwoDeadEndInTrainPath";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("t");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("t", "railway.Train"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_t = body.getOrCreateVariableByName("t");
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_t, "t")
      	));
      	new TypeUnary(body, var_e1, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	new TypeUnary(body, var_e2, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	new Inequality(body, var_e1, var_e2);
      	new TypeBinary(body, CONTEXT, var_t, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	new TypeBinary(body, CONTEXT, var_t, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
