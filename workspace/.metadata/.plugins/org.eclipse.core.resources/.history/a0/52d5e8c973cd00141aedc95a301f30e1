package hu.bme.mit.mdsd.railway;

import hu.bme.mit.mdsd.railway.util.TrainsAreNotOnNodesQuerySpecification;
import java.util.Arrays;
import java.util.List;
import org.eclipse.incquery.runtime.api.IPatternMatch;
import org.eclipse.incquery.runtime.api.impl.BasePatternMatch;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import railway.Node;

/**
 * Pattern-specific match representation of the hu.bme.mit.mdsd.railway.trainsAreNotOnNodes pattern,
 * to be used in conjunction with {@link TrainsAreNotOnNodesMatcher}.
 * 
 * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
 * Each instance is a (possibly partial) substitution of pattern parameters,
 * usable to represent a match of the pattern in the result of a query,
 * or to specify the bound (fixed) input parameters when issuing a query.
 * 
 * @see TrainsAreNotOnNodesMatcher
 * @see TrainsAreNotOnNodesProcessor
 * 
 */
@SuppressWarnings("all")
public abstract class TrainsAreNotOnNodesMatch extends BasePatternMatch {
  private Node fNodes;
  
  private static List<String> parameterNames = makeImmutableList("nodes");
  
  private TrainsAreNotOnNodesMatch(final Node pNodes) {
    this.fNodes = pNodes;
  }
  
  @Override
  public Object get(final String parameterName) {
    if ("nodes".equals(parameterName)) return this.fNodes;
    return null;
  }
  
  public Node getNodes() {
    return this.fNodes;
  }
  
  @Override
  public boolean set(final String parameterName, final Object newValue) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    if ("nodes".equals(parameterName) ) {
    	this.fNodes = (railway.Node) newValue;
    	return true;
    }
    return false;
  }
  
  public void setNodes(final Node pNodes) {
    if (!isMutable()) throw new java.lang.UnsupportedOperationException();
    this.fNodes = pNodes;
  }
  
  @Override
  public String patternName() {
    return "hu.bme.mit.mdsd.railway.trainsAreNotOnNodes";
  }
  
  @Override
  public List<String> parameterNames() {
    return TrainsAreNotOnNodesMatch.parameterNames;
  }
  
  @Override
  public Object[] toArray() {
    return new Object[]{fNodes};
  }
  
  @Override
  public TrainsAreNotOnNodesMatch toImmutable() {
    return isMutable() ? newMatch(fNodes) : this;
  }
  
  @Override
  public String prettyPrint() {
    StringBuilder result = new StringBuilder();
    result.append("\"nodes\"=" + prettyPrintValue(fNodes)
    );
    return result.toString();
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fNodes == null) ? 0 : fNodes.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
    	return true;
    if (!(obj instanceof TrainsAreNotOnNodesMatch)) { // this should be infrequent
    	if (obj == null) {
    		return false;
    	}
    	if (!(obj instanceof IPatternMatch)) {
    		return false;
    	}
    	IPatternMatch otherSig  = (IPatternMatch) obj;
    	if (!specification().equals(otherSig.specification()))
    		return false;
    	return Arrays.deepEquals(toArray(), otherSig.toArray());
    }
    TrainsAreNotOnNodesMatch other = (TrainsAreNotOnNodesMatch) obj;
    if (fNodes == null) {if (other.fNodes != null) return false;}
    else if (!fNodes.equals(other.fNodes)) return false;
    return true;
  }
  
  @Override
  public TrainsAreNotOnNodesQuerySpecification specification() {
    try {
    	return TrainsAreNotOnNodesQuerySpecification.instance();
    } catch (IncQueryException ex) {
     	// This cannot happen, as the match object can only be instantiated if the query specification exists
     	throw new IllegalStateException (ex);
    }
  }
  
  /**
   * Returns an empty, mutable match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @return the empty match.
   * 
   */
  public static TrainsAreNotOnNodesMatch newEmptyMatch() {
    return new Mutable(null);
  }
  
  /**
   * Returns a mutable (partial) match.
   * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
   * 
   * @param pNodes the fixed value of pattern parameter nodes, or null if not bound.
   * @return the new, mutable (partial) match object.
   * 
   */
  public static TrainsAreNotOnNodesMatch newMutableMatch(final Node pNodes) {
    return new Mutable(pNodes);
  }
  
  /**
   * Returns a new (partial) match.
   * This can be used e.g. to call the matcher with a partial match.
   * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
   * @param pNodes the fixed value of pattern parameter nodes, or null if not bound.
   * @return the (partial) match object.
   * 
   */
  public static TrainsAreNotOnNodesMatch newMatch(final Node pNodes) {
    return new Immutable(pNodes);
  }
  
  private static final class Mutable extends TrainsAreNotOnNodesMatch {
    Mutable(final Node pNodes) {
      super(pNodes);
    }
    
    @Override
    public boolean isMutable() {
      return true;
    }
  }
  
  private static final class Immutable extends TrainsAreNotOnNodesMatch {
    Immutable(final Node pNodes) {
      super(pNodes);
    }
    
    @Override
    public boolean isMutable() {
      return false;
    }
  }
}
