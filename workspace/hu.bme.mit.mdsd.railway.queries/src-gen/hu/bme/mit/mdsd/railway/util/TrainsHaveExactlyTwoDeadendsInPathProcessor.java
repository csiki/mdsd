package hu.bme.mit.mdsd.railway.util;

import hu.bme.mit.mdsd.railway.TrainsHaveExactlyTwoDeadendsInPathMatch;
import org.eclipse.incquery.runtime.api.IMatchProcessor;
import railway.Train;

/**
 * A match processor tailored for the hu.bme.mit.mdsd.railway.trainsHaveExactlyTwoDeadendsInPath pattern.
 * 
 * Clients should derive an (anonymous) class that implements the abstract process().
 * 
 */
@SuppressWarnings("all")
public abstract class TrainsHaveExactlyTwoDeadendsInPathProcessor implements IMatchProcessor<TrainsHaveExactlyTwoDeadendsInPathMatch> {
  /**
   * Defines the action that is to be executed on each match.
   * @param pT the value of pattern parameter t in the currently processed match
   * 
   */
  public abstract void process(final Train pT);
  
  @Override
  public void process(final TrainsHaveExactlyTwoDeadendsInPathMatch match) {
    process(match.getT());
  }
}
