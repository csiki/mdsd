package hu.bme.mit.mdsd.railway.util;

import com.google.common.collect.Sets;
import hu.bme.mit.mdsd.railway.IsAtLeastThreeDeadEndInTrainPathMatch;
import hu.bme.mit.mdsd.railway.IsAtLeastThreeDeadEndInTrainPathMatcher;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.eclipse.incquery.runtime.api.IncQueryEngine;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.incquery.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.incquery.runtime.exception.IncQueryException;
import org.eclipse.incquery.runtime.matchers.psystem.PBody;
import org.eclipse.incquery.runtime.matchers.psystem.PVariable;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.incquery.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeBinary;
import org.eclipse.incquery.runtime.matchers.psystem.basicenumerables.TypeUnary;
import org.eclipse.incquery.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.incquery.runtime.matchers.psystem.queries.QueryInitializationException;

/**
 * A pattern-specific query specification that can instantiate IsAtLeastThreeDeadEndInTrainPathMatcher in a type-safe way.
 * 
 * @see IsAtLeastThreeDeadEndInTrainPathMatcher
 * @see IsAtLeastThreeDeadEndInTrainPathMatch
 * 
 */
@SuppressWarnings("all")
public final class IsAtLeastThreeDeadEndInTrainPathQuerySpecification extends BaseGeneratedEMFQuerySpecification<IsAtLeastThreeDeadEndInTrainPathMatcher> {
  private IsAtLeastThreeDeadEndInTrainPathQuerySpecification() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws IncQueryException if the pattern definition could not be loaded
   * 
   */
  public static IsAtLeastThreeDeadEndInTrainPathQuerySpecification instance() throws IncQueryException {
    try{
    	return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
    	throw processInitializerError(err);
    }
  }
  
  @Override
  protected IsAtLeastThreeDeadEndInTrainPathMatcher instantiate(final IncQueryEngine engine) throws IncQueryException {
    return IsAtLeastThreeDeadEndInTrainPathMatcher.on(engine);
  }
  
  @Override
  public IsAtLeastThreeDeadEndInTrainPathMatch newEmptyMatch() {
    return IsAtLeastThreeDeadEndInTrainPathMatch.newEmptyMatch();
  }
  
  @Override
  public IsAtLeastThreeDeadEndInTrainPathMatch newMatch(final Object... parameters) {
    return IsAtLeastThreeDeadEndInTrainPathMatch.newMatch((railway.Train) parameters[0]);
  }
  
  private static class LazyHolder {
    private final static IsAtLeastThreeDeadEndInTrainPathQuerySpecification INSTANCE = make();
    
    public static IsAtLeastThreeDeadEndInTrainPathQuerySpecification make() {
      return new IsAtLeastThreeDeadEndInTrainPathQuerySpecification();					
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private final static IsAtLeastThreeDeadEndInTrainPathQuerySpecification.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    @Override
    public String getFullyQualifiedName() {
      return "hu.bme.mit.mdsd.railway.isAtLeastThreeDeadEndInTrainPath";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("t");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return Arrays.asList(new PParameter("t", "railway.Train"));
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() throws QueryInitializationException {
      Set<PBody> bodies = Sets.newLinkedHashSet();
      try {
      {
      	PBody body = new PBody(this);
      	PVariable var_t = body.getOrCreateVariableByName("t");
      	PVariable var_e1 = body.getOrCreateVariableByName("e1");
      	PVariable var_e2 = body.getOrCreateVariableByName("e2");
      	PVariable var_e3 = body.getOrCreateVariableByName("e3");
      	body.setExportedParameters(Arrays.<ExportedParameter>asList(
      		new ExportedParameter(body, var_t, "t")
      	));
      	new TypeUnary(body, var_e1, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	new TypeUnary(body, var_e2, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	new TypeUnary(body, var_e3, getClassifierLiteral("hu.bme.mit.mdsd.railway", "Deadend"), "hu.bme.mit.mdsd.railway/Deadend");
      	new Inequality(body, var_e1, var_e2);
      	new Inequality(body, var_e1, var_e3);
      	new Inequality(body, var_e2, var_e3);
      	new TypeBinary(body, CONTEXT, var_t, var_e1, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	new TypeBinary(body, CONTEXT, var_t, var_e2, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	new TypeBinary(body, CONTEXT, var_t, var_e3, getFeatureLiteral("hu.bme.mit.mdsd.railway", "Train", "route"), "hu.bme.mit.mdsd.railway/Train.route");
      	bodies.add(body);
      }
      	// to silence compiler error
      	if (false) throw new IncQueryException("Never", "happens");
      } catch (IncQueryException ex) {
      	throw processDependencyException(ex);
      }
      return bodies;
    }
  }
}
